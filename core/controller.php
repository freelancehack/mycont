<?php

class controller
{
    /**
     * contrllerの名前
     */
    public $name = null;
    /**
     * action名
     */
    public $action = null;
    /**
     * action名
     */
    public $actionFailed = false;
    /**
     * useModels
     */
    public $useModels = [];
    /**
     * view
     */
    public $view = null;
    /**
     * viewModel
     */
    public $viewModel = [];
    /**
     * 許可しているcontroller　& action 名
     */
    const ALLOWED_ROUTE_STR_REG = '[a-z][a-z_]*[a-z]';

/*
	// beforeFilter
	abstract protected function beforeFilter();
	// afterFilter
	abstract protected function afterFilter();
*/
    protected function toCamel($str)
    {
        return str_replace('_', '', ucwords(strtolower($str), '_'));
    }

    protected function toSnake($str)
    {
        $str = preg_replace('![A-Z]!', '_\0', $str);
        return strtolower(ltrim($str, '_'));
    }

    /**
     *
     */
    protected function require_once($filename, $className)
    {
        if (!file_exists($filename)) {
            if (TPF_DEBUG) {
                echo "$filename is not exist.";
            }
            return false;
        }
        /*
		if (!runkit_lint_file($filename)) {
			if (TPF_DEBUG) echo "$filename has some syntax error.";
			return false;
		}
		*/
        require_once($filename);
        if (!class_exists($className)) {
            if (TPF_DEBUG) {
                echo "There is no $className class.";
            }
            return false;
        }
        return true;
    }

    /**
     *
     */
    protected function build(&$controller)
    {
        if ($controller->view) {
            $controller->viewModel = new $controller->view($controller);
        }
        if (count($controller->useModels)) {
            foreach ($controller->useModels as $index => $model) {
                if (is_numeric($index)) {
                    $this->buildModel($controller, $model);
                } else {
                    $this->buildModel($controller, $index);
                    if (!is_array($model)) $model = [$model];
                    foreach ($model as $exModel) {
                        $this->buildModel($controller, $exModel, $index);                    
                    }                    
                }
            }
        }
    }

    /**
     *
     */
    protected function buildModel(&$controller, $model, $parentModel = null)
    {
        $exDir = $parentModel ? $this->toSnake($parentModel). '/' : null;
            $filename = TPF_MODELS_DIR. $exDir. $this->toSnake($model). '.php';
        if (!$this->require_once($filename, $parentModel. $model)) {
            return;
        }
        $newModel = $parentModel. $model;
        $controller->$newModel = new $newModel();
        if (count($controller->$newModel->useDaos)) {
            foreach ($controller->$newModel->useDaos as $dao) {
                $this->buildDao($controller->$newModel, $dao);
            }
        }
    }

    /**
     *
     */
    protected function buildDao(&$model, $dao)
    {
        $filename = TPF_DAOS_DIR. $this->toSnake($dao). '.php';
        if (!$this->require_once($filename, $dao)) {
            return;
        }
        $model->$dao = new $dao();
    }
}
