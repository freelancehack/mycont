<?php

class Dao
{
    protected $table = null;
    protected $offset = 0;
    protected $div = 20;

    public function db()
    {
        global $wpdb;
        if (TPF_DEBUG) {
            $wpdb->show_errors();
        }
        return $wpdb;
    }

    public function __call($name, $arguments)
    {
        $value = $arguments[0];
        $postQuery = !empty($arguments[1]) ? $arguments[1] : null;
        $calc = !empty($arguments[2]) ? true : false;

        if (preg_match('!^(find|get)(|[A-Z][a-zA-Z]+?)By([A-Z][a-zA-Z]+)!', $name, $match)) {
            if ($match[3] === 'ID') {
                $column = 'ID';
            } else {
                $column = strtolower(ltrim(preg_replace('![A-Z]!', '_\0', $match[3]), '_'));
            }
            $simpleFkeys = ($match[1] === 'get') ? $this->simpleFkeys : null ;
            if (empty($match[2])) {
                return $this->getResultsByColumn($column, $value, $simpleFkeys, $postQuery, $calc);            

            } else {
                $targetName = strtolower(ltrim(preg_replace('![A-Z]!', '_\0', $match[2]), '_')) ;
                return $this->getFieldByColumn($targetName, $column, $value, $simpleFkeys, $postQuery, $calc);            
            }
        }
    }


    public function find($fields, $postQuery)
    {
        $query =
            "
                SELECT {$fields} FROM {$this->table}
                {$postQuery} ;
            ";
        return $this->execute($query);
    }

    protected function execute($query)
    {
        return $this->db()->get_results($query, ARRAY_A);
    }

    public function count($conditions, $countColumn = 'id')
    {
        $tmpWhere = [];
        foreach ($conditions as $name => $value) {
            $tmpWhere[] = "{$name} = '{$value}'";
        }
        $where = implode(' AND ', $tmpWhere);
        $query = "SELECT count({$countColumn}) FROM {$this->table} WHERE {$where}";
        return $this->db()->get_var($query);
    }


    public function insert($data, $cast = null)
    {
        if ($this->db()->insert($this->table, $data, $cast)) {
            return $this->lastInsertId();
        }
        return false;
    }

    public function update($sets, $conditions, $castSet = null, $castCond = null)
    {
        return $this->db()->update($this->table, $sets, $conditions, $castSet, $castCond);
    }

    public function foundRows()
    {
        return $this->db()->get_var('SELECT FOUND_ROWS()');
    }

    public function lastInsertId()
    {
        return $this->db()->insert_id;
    }
    public function div()
    {
        return $this->div;
    }

    protected function getResultsByColumn($column, $arg, $simpleFkeys, $postQuery = null, $calc = null)
    {

        $leftJoin = null;
        $fkeyAliases = null;
        if ($simpleFkeys) foreach($simpleFkeys as $fkey => $val) {
            list($joinTable, $joinColumn) = explode('.', $val[0]);
            $leftJoin .= "
                LEFT JOIN
                {$joinTable}
                ON
                {$this->table}.{$fkey} = {$val[0]}
            ";
            $fkeyAliases .= "
                , {$joinTable}.{$val[1]} as {$joinTable}_{$val[1]}
            ";
        }
        if ($calc) {
            $calc = ' SQL_CALC_FOUND_ROWS ';
        }
        return $this->db()->get_results(
            "
                SELECT {$calc} {$this->table}.* {$fkeyAliases} FROM {$this->table}
                {$leftJoin}
                WHERE {$this->table}.{$column} = '{$arg}'
                {$postQuery}
            ", 
            ARRAY_A);


    }
    protected function getFieldByColumn($targetName, $column, $arg, $simpleFkeys, $postQuery = null)
    {
        $result = $this->getResultsByColumn($column, $arg, $simpleFkeys, $postQuery);
        if (empty($result)) {
            return $result;
        }

        $ret = [];
        foreach ($result as $index => $value) {
            if (!empty($value['id'])) {
                $index = $value['id'];
            }
            $ret[$index] = $value[$targetName];
        }
        return $ret;
    }

    protected function orderStmt($sorts)
    {
        $orderConditions = [];
        foreach ($sorts as $key => $value) {
            $orderConditions[] = "{$this->table}.{$key} {$value}";
        }
        return 'ORDER BY '. implode(', ', $orderConditions);
    }

}
