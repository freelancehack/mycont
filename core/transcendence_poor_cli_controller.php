<?php

class TranscendencePoorCliController extends CliController
{

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->setRoute();
        $this->executeMethod();
    }

    /**
     * fetch
     */
    public function fetch()
    {
    }

    /**
     * setRoute
     * SCRIPT_NAMEからcallするcontroller/actionを特定し、controllerファイルをrequireする
     *
     */
    private function setRoute()
    {
        $rs = self::ALLOWED_ROUTE_STR_REG;
        $this->parseRequest();
        $tpf = 'tpf';
        // ファイルを特定してrequire_once
        $filename = TPF_CLI_CONTROLLERS_DIR. $this->controller. '.php';
        $className = $this->toCamel($this->controller). 'Controller';
        if (!$this->require_once($filename, $className)) {
            $this->notFound();
        }
    }

    /**
     * executeMethod
     * 指定actionの実行
     */
    private function executeMethod()
    {
        $controllerClassName = $this->toCamel($this->controller). 'Controller';
        // インスタンス生成
        if (class_exists($controllerClassName)) {
            $CliController = new $controllerClassName($this);
        } else {
            return;
        }
        // actionするメソッドの確認
        $methodName = lcfirst($this->toCamel($this->action));

        if (!method_exists($CliController, $methodName)) {
            $this->notFound();
            return;
        }
        // controllerが利用する model / dao などをbuildする
        $this->build($CliController);
        // 指定actionの前処理実施
        $CliController->beforeFilter();
        // 指定action実施
        call_user_func_array([$CliController, $methodName], $this->argv);
        // 指定actionの後処理実施
        $CliController->afterFilter();
    }

    private function notFound()
    {
        echo "not found : {$this->controller}::{$this->action}";
    }
}
