<?php

class CliController extends controller
{
    public $argv = [];

    public function __construct($controller)
    {
        $this->name = $controller->controller;
        $this->action = $controller->action;
    }

    protected function parseRequest()
    {
        $argv = $_SERVER['argv'];
        array_shift($argv);
        @list($this->controller, $this->action) = explode('/', $argv[0]);
        if (empty($this->action)) {
            $this->action = 'main';
        }
        array_shift($argv);
        if (count($argv)) foreach ($argv as $val) {
            if ($val == 'null') {
                $val = null;
            }
            $this->argv[] = $val;
        }
    }

    protected function beforeFilter()
    {
    }

    protected function afterFilter()
    {
    }
}
