<?php

class WebController extends controller
{
    /**
     * $_GET
     */
    public $url = null;
    /**
     * http header and body. viewで作成される。
     */
    public $response = ['http_header'=> '', 'body' => 'output'];
    /**
     * viewのlayout
     */
    public $layout = 'default';
    /**
     * viewのlayout
     */
    public $pageTpl = null;
    /**
     * view
     */
    public $view = 'View';

    public function __construct($controller)
    {
        $this->name = $controller->controller;
        $this->action = $controller->action;
        $this->url = $controller->url;
    }

    protected function parseRequest()
    {
        $url = @$_SERVER['REQUEST_URI'];
        preg_match('!^'. TPF_BASE_URL. '([^?]*)(\?|)(.*)$!', $url, $match);
        $this->url = ltrim($match[1], '/');
    }

    protected function set($vars)
    {
        $this->viewModel->setViewVars($vars);
    }

    public function getRender()
    {
        return $this->url;
    }

    protected function render($tpl = null)
    {
        if (empty($this->pageTpl)) {
            $this->pageTpl = $this->url;
        }
        if (empty($tpl)) {
            return $this->pageTpl;
        }
        $this->pageTpl = $tpl;
    }

    protected function beforeFilter()
    {
    }

    protected function afterFilter()
    {
    }
}
