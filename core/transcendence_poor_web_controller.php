<?php

class TranscendencePoorWebController extends WebController
{

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->setRoute();
        $this->executeMethod();
    }

    /**
     * fetch
     */
    public function fetch()
    {
        if (!empty($this->response['http_header'])) {
            header($this->response['http_header']);
        }
        echo $this->response['body'];
    }

    /**
     * setRoute
     * SCRIPT_NAMEからcallするcontroller/actionを特定し、controllerファイルをrequireする
     *
     */
    private function setRoute()
    {
        $rs = self::ALLOWED_ROUTE_STR_REG;
        // urlから　controller/actionを特定
        $this->parseRequest();
        $tpf = 'tpf';
        if (empty($this->url)) {
            $this->controller = 'default';
            $this->action = 'index';
            $this->pageTpl = $this->url = 'default/index';
        } elseif (preg_match("!^({$rs})/?({$rs}|)!", $this->url, $match)) {
            $this->controller = $match[1];
            $this->action = $match[2];
            if (empty($match[2])) {
                $this->action = 'index';
                $this->url = $this->controller. '/'. $this->action;
            }
        } else {
            $this->notFound404();
            return;
        }
        // ファイルを特定してrequire_once
        $filename = TPF_CONTROLLERS_DIR. $this->controller. '.php';
        $className = $this->toCamel($this->controller). 'Controller';
        if (!$this->require_once($filename, $className)) {
            $this->notFound404();
        }
    }

    /**
     * executeMethod
     * 指定actionの実行
     */
    private function executeMethod()
    {
        $controllerClassName = $this->toCamel($this->controller). 'Controller';
        // インスタンス生成
        if (class_exists($controllerClassName)) {
            $webController = new $controllerClassName($this);
        } else {
            return;
        }
        // actionするメソッドの確認
        $methodName = lcfirst($this->toCamel($this->action));

        if (!method_exists($webController, $methodName)) {
            return;
        }
        // controllerが利用する model / dao などをbuildする
        $this->build($webController);
        // 指定actionの前処理実施
        $webController->beforeFilter();
        // 指定action実施
        $webController->$methodName();
        // contentのrender
        $pageTpl = $webController->render();
        $content = $webController->viewModel->render($pageTpl, TPF_PAGES_DIR);
        $webController->set(compact('content', 'pageTpl'));
        // 指定actionの後処理実施
        $webController->afterFilter();
        // responseの取得
        $this->response['header'] = $webController->response['http_header'];
        $this->response['body'] = $webController->viewModel->render($webController->layout, TPF_LAYOUTS_DIR);
    }

    private function notFound404()
    {
        $this->controller = 'error';
        $this->action = 'not_found';
        $this->pageTpl = 'error/not_found';

        $filename = TPF_CONTROLLERS_DIR. $this->controller. '.php';
        $className = $this->toCamel($this->controller). 'Controller';
        $this->require_once($filename, $className);
    }
}
