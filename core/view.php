<?php

class View
{
    /**
     * テンプレートをincludeすう際にextractされる変数
     */
    protected $viewVars = [];

    /**
     *
     */
    public function setViewVars($vars)
    {
        $this->viewVars = array_merge($this->viewVars, $vars);
    }

    public function render($_tpl, $_dir = TPF_PAGES_DIR)
    {
        extract($this->viewVars);
        $_filename = $_dir. $_tpl. '.php';
        ob_start();
        if (TPF_DEBUG && $dir == TPF_PAGES_DIR) {
            echo "\n<!-- start render $_tpl -->\n";
        }
        @include($_filename);
        if (!preg_match('!json|xml!', $_tpl) && TPF_DEBUG) {
            echo "\n<!-- end render $_tpl -->\n";
        }
        return ob_get_clean();
        ;
    }

    public function element($_tpl, $_vars = [])
    {
        extract(array_merge($this->viewVars, $_vars));
        $_filename = TPF_ELEMENTS_DIR. $_tpl. '.php';
        ob_start();
        if (TPF_DEBUG) {
            echo "\n<!-- start element $_tpl -->\n";
        }
        @include($_filename);
        if (TPF_DEBUG) {
            echo "\n<!-- end element $_tpl -->\n";
        }
        return ob_get_clean();
        ;
    }
}
