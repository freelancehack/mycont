<?php

/**
 * TPF - Transcendence Poor PHP Framework For WordPress
 *
 * @package  TPF
 * @author   sugoiyo72 <sugoiyo72@ms.sugoiyo.com>
 */

/**
 * 定数展開用関数
 */
function tpf($v)
{
    return $v;
}

if (!defined('TPF_BASE_PATH')) {
    echo 'Setup index.php and define TPF_BASE_DIR';
    exit;
}
/*
 * APP DIRS
 */
$tpf = 'tpf';
//
if (!defined('TPF_APP_DIR')) {
    define('TPF_APP_DIR', "{$tpf(TPF_BASE_PATH)}app/");
}
//
define('TPF_CONTROLLERS_DIR', "{$tpf(TPF_APP_DIR)}controllers/");
//
define('TPF_CLI_CONTROLLERS_DIR', "{$tpf(TPF_APP_DIR)}cli_controllers/");
//
define('TPF_DAOS_DIR', "{$tpf(TPF_APP_DIR)}daos/");
//
define('TPF_MODELS_DIR', "{$tpf(TPF_APP_DIR)}models/");
//
define('TPF_VIEWS_DIR', "{$tpf(TPF_APP_DIR)}views/");
//
define('TPF_LAYOUTS_DIR', "{$tpf(TPF_VIEWS_DIR)}layouts/");
//
define('TPF_PAGES_DIR', "{$tpf(TPF_VIEWS_DIR)}pages/");
//
define('TPF_ELEMENTS_DIR', "{$tpf(TPF_VIEWS_DIR)}elements/");

/**
 *
 */
$tpfSapi = (php_sapi_name() == 'cli') ? 'cli' : 'web';
$tpfExcludeSapi = (php_sapi_name() == 'cli') ? 'web' : 'cli';
$tpfControllerName = "transcendence_poor_{$tpfSapi}_controller";
define(
    'TPF_REQUIRES',
    [
        TPF_CORE_DIR. 'controller.php',
        TPF_CORE_DIR. "{$tpfSapi}_controller.php",
        TPF_CORE_DIR. "{$tpfControllerName}.php",
        TPF_CORE_DIR. 'auth.php',
        TPF_CORE_DIR. 'dao.php',
        TPF_CORE_DIR. 'view.php'
    ]
);

foreach (TPF_REQUIRES as $tpfRequireFile) {
    require_once($tpfRequireFile);
}

if (glob(TPF_APP_DIR.'app_*.php')) {
    foreach (glob(TPF_APP_DIR.'app_*.php') as $filename) {
        if (preg_match('!/app_'.$tpfExcludeSapi.'!', $filename)) {
            continue;
        }
        require_once($filename);
    }
}

$tpfControllerName = str_replace('_', '', ucwords(strtolower($tpfControllerName), '_'));
