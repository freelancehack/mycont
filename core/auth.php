<?php

class Auth
{
    public static function user()
    {
        global $current_user;
        return $current_user;
    }

    public static function userData()
    {
        $user = self::user();
        return [
            'ID' => $user->ID,
            'display_name' => $user->data->display_name,
            'email' => $user->data->user_email,
        ];
    }

    public static function userId()
    {
        return self::user()->ID;
    }

    public static function check()
    {
        if (!is_user_logged_in()) {
            auth_redirect();
        }
    }

    public static function isAdmin()
    {
        return @self::user()->caps['administrator'];
    }

    public static function isEditor()
    {
        return @self::user()->caps['contributor'] || self::isAdmin();
    }

    public static function isPartner()
    {
        return @self::user()->caps['subscriber'];
    }
}
