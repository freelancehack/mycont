<?php

class AppModel
{

    public function pagination($total, $div, $request, $count)
    {
        $currentPage = $request['page'];
        $prevPage = ((int)$currentPage > 1) ? ($currentPage - 1) : null;
        $nextPage = (($currentPage * $div) < $total) ? $currentPage + 1: null;
        $startNum = ($currentPage - 1) * $div + 1;
        $endNum = ($currentPage - 1) * $div + $count;
        $lastPage = ceil($total / $div);
        if ($currentPage + 4 > $lastPage) {
            $lastPage = null;
        }
        return [
            'total' => $total,
            'current' => "$startNum - $endNum",
            'pages' => [
                'prev' => $prevPage,
                'prevUrl' => $this->pageUrl($request, $prevPage),
                'current' => $currentPage,
                'next' => $nextPage,
                'nextUrl' => $this->pageUrl($request, $nextPage),
                'last' => $lastPage,
                'lastUrl' => $this->pageUrl($request, $lastPage),
            ]
        ];
    }

    private function pageUrl($request, $page)
    {
        $rets = [];
        $request['page'] = $page;
        foreach ($request as $key => $value) {
            $rets[] = "{$key}={$value}";
        }
        return '?'. join('&', $rets);
    }
}
