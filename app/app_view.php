<?php

class AppView extends View
{
    public $controller;

    protected $defaultStylesheets = [
        '//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css',
        '/wp-content/themes/biz-vektor/css/bizvektor_common_min.css',
        '/wp-content/themes/biz-vektor/design_skins/003/css/003.css',
        TPF_BASE_URL. '/assets/css/common/common.css',
    ];

    protected $defaultScripts = [
        //'//code.jquery.com/jquery-2.2.0.min.js',
        TPF_BASE_URL. '/assets/js/common/common.js',
    ];

    public function __construct($controller)
    {
        $this->controller = $controller;
        $this->AppHelper = new AppHelper();
    }

    public function stylesheets($pageTpl)
    {
        $ret = '';
        foreach ($this->defaultStylesheets as $style) {
            $ret .= $this->css($style);
        }
        if ($this->controller->autoLinks['css'][$pageTpl])  foreach ($this->controller->autoLinks['css'][$pageTpl] as $filename) {
            $ret .= $this->css($filename);
        }
        $filename = TPF_BASE_URL. '/assets/css/pages/'. $pageTpl. '.css';
        $ret .= $this->css($filename);
        return $ret;
    }

    public function css($style)
    {
        if (!preg_match('!^/\w!', $style)) {
            return $this->element('common/tags/stylesheet', ['href' => $style]);
        }
        $filename = ABSPATH. $style;
        if (!file_exists($filename)) {
            if (TPF_DEBUG) {
                return "<!-- Notice! $style is not exist. -->\n";
            }
            return '';
        }
        return $this->element('common/tags/stylesheet', ['href' => $style. '?'. filemtime($filename)]);
    }

    public function scripts($pageTpl)
    {
        $ret = '';
        foreach ($this->defaultScripts as $script) {
            $ret .= $this->script($script, false);
        }
        if ($this->controller->autoLinks['js'][$pageTpl])  foreach ($this->controller->autoLinks['js'][$pageTpl] as $filename) {
            $ret .= $this->script($filename);
        }

        $filename = TPF_BASE_URL. '/assets/js/pages/'. $pageTpl. '.js';
        $ret .= $this->script($filename);
        return $ret;
    }
    public function script($script, $async = true)
    {
        if (!preg_match('!^/\w!', $script)) {
            return $this->element('common/tags/script', ['src' => $script, 'async' => $async]);
        }
        $filename = ABSPATH. $script;
        if (!file_exists($filename)) {
            if (TPF_DEBUG) {
                return "<!-- Notice! $script is not exist. -->\n";
            }
            return '';
        }
        return $this->element('common/tags/script', ['src' => $script. '?'. filemtime($filename)]);
    }


    public function chartRender($data, $options = null, $htmlOptions = null)
    {
        $htmlOptions = Set::merge(
            array(
                'divId' => null,
                'class' => array(),
                'css' => array(
                    'width' => '330px',
                    'height' => '220px'
                )
            ),
            $htmlOptions
        );

        /* Process data */
        if (!empty($data['labels']) && !empty($data['values'])) {
            // Process X-Axis ticks
            $ticks = array();
            foreach ($data['labels'] as $key => $tick) {
                $ticks[$key] = array($key, $tick);
            }

            // Convert all numeric values in string to float
            foreach ($data['values'] as $key => $values) {
                foreach ($values as &$value) {
                    $value = floatval($value);
                }
                $data['values'][$key] = $values;
            }

            // Calculate and set bar width
            $countDataPoint = count($data['labels']);
            $countDataPoint = $countDataPoint > 0 ? $countDataPoint : 1;
            $containerWidth = filter_var($htmlOptions['css']['width'], FILTER_SANITIZE_NUMBER_INT);
            // $barOuterWidth is included of bar width and bar margin
            $barOuterWidth = $containerWidth / $countDataPoint;
            // $barMargin accounted 30% of $barOuterWidth
            $barMargin = $barOuterWidth * 0.3;
            // $barMargin accounted 70% of $barOuterWidth
            $barWidth = $barOuterWidth - $barMargin;
            $this->options['seriesDefaults']['rendererOptions']['barWidth'] = $barWidth;

            $this->options = Set::merge(
                $this->options,
                array(
                    'axes' => array(
                        'xaxis' => array('ticks' => $ticks)
                    )
                )
            );
        }

        /* Render HTML code */
        return $this->getRendering($data, $htmlOptions);
    }
}
