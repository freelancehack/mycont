<?php

class RawsController extends AppWebController
{
    public $useModels = ['Bookmark' => ['Raw'], 'SourceDomain'];

    public $requestParams = [
    	'index' => [
    		'domain',
    		'review_status',
    	]
    ];

    public function index()
    {
        $raws = $this->BookmarkRaw->raws($this->request);
        $this->set(compact('raws'));
    }
}
