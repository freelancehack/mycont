<?php

class ContributesController extends AppWebController
{
    public $useModels = ['Bookmark' => ['Contribute'], 'User'];

    public $requestParams = [
        'index' => [
            'id',
            'category_id' => ['default' => 49],
            'crawler_id'  => ['default' => 1],
            'page'  => ['min' => 1],
            'user_id',
            'recommended'
        ],
        'confirm' => [
            'url'
        ],
        'to_review' => [
            'id',
            'url',
            'title',
            'category_id',
            'summary',
            'noncapture'
        ],
        'publish' => [
            'id',
            'url',
            'title',
            'category_id',
            'summary',
            'noncapture'
        ],
    ];


    public function index()
    {
        if (!empty($this->request['id'])) {
            $this->request = [
                'id' => $this->request['id'],
                'category_id' => '',
                'crawler_id' => '',
                'page' => '1'
            ];

        }

        $this->set(['pageTitle' => '投稿']);
        list($candidates, $pagination) = $this->Bookmark->candidates($this->request);
        $isAllowedPublish = $this->User->isAllowedPublish(Auth::userID());
        $request = $this->request;
        $selectElement = 'bookmark';

        $this->set(
            compact(
                'candidates',
                'request',
                'pagination',
                'isAllowedPublish',
                'selectElement'));
    }
    public function confirm()
    {
        $this->layout = 'json';
        $reusult = $this->Bookmark->existBookmarkedPost($this->request['url']);
        if (count($result) == 0) {
            $json = array (
                'status' => 'success',
                'message' => '新規投稿です'
            );
        } else {
            $json = array (
                'status' => 'warning',
                'message' => count($result) .'件のデータが存在します'
            );
        }
        $this->set(compact('json'));
    }
    public function toReview()
    {
        $this->layout = 'json';
        $json = $this->checkUrl($this->request['url']);
        if (!$json) {
            $json = $this->Bookmark->registBookmark($this->request, 'pending', Auth::userID());
        }
        $this->set(compact('json'));
    }

    public function publish()
    {
        $this->layout = 'json';
        $json = $this->checkUrl($this->request['url']);
        if (!$json) {
            $json = $this->Bookmark->registBookmark($this->request, 'publish', Auth::userID());
        }
        $this->set(compact('json'));
    }
    private function checkUrl()
    {
        $url = $this->request['url'];
        if (empty($url)
            || !preg_match('!https?://!', $url)) {
            return [
                'status' => 'error',
                'message' => 'URLが正しくありません'
            ];
        }
        if (preg_match('!\?!', $url)) {
            return [
                'status' => 'error',
                'message' => 'URLに「?」があります。クエリ付きURLは受け付けません'
            ];
        }
        return null;
    }
}
