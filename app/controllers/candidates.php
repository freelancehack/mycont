<?php

class CandidatesController extends AppWebController
{
    public $useModels = ['Bookmark' => ['Candidate']];

    public $requestParams = [
        'regist' => [
            'title' => ['func' => 'urldecode'],
            'url',
            'summary' => ['func' => 'urldecode']
        ],
        'index' => [
            'id',
            'category_id' => ['default' => 49],
            'crawler_id'  => ['default' => 1],
            'page'  => ['min' => 1],
            'user_id',
            'recommended'
        ],
        'pass' => [
            'id',
        ],
        'change' => [
            'id',
            'column',
            'value'
        ],
        'reserve' => [
            'id',
        ],
    ];

    protected function error($msg)
    {
        $this->render('error/index');
        $this->set(compact('msg'));
    }

    public function regist()
    {
        if (empty($this->request['title']) || empty($this->request['url'])) {
            $this->error('ブックマーク情報が送信されませんでした。');
            return;
        }

        if (!preg_match('!https?://!', $this->request['url'])) {
            $this->error('URLが正しくありません');
            return;
        }
        if (preg_match('!\?!', $this->request['url'])) {
            $this->error('URLに「?」があります。クエリ付きURLは受け付けません');
            return;
        }

        list($id, $msg) = $this->BookmarkCandidate->registCandidate($this->request);
        if ($msg) {
            $this->error($msg);
            return;
        }
    }

    /**
     * ブックマーク候補一覧
     */
    public function index()
    {
        $request = $this->request;
        $this->set(['pageTitle' => 'ブックマーク候補一覧']);
        list($candidates, $pagination)
            = $this->BookmarkCandidate->candidates($request);

        if (!empty($this->request['id']) && empty($candidates)) {
            header('Location: /bkmks/bookmark_info/?id='. $request['id']);
            return;
        }
        $selectElement = 'bookmark';
        $this->set(
            compact(
                'candidates',
                'request',
                'pagination',
                'selectElement'
            )
        );
    }
    /**
     * 候補のパス
     */
    public function pass()
    {
        $this->layout = 'json';
        $json = $this->BookmarkCandidate->pass($this->request, Auth::userID());
        $this->set(compact('json'));
    }
    /**
     * ステータスの変更
     */
    public function change()
    {
        $this->layout = 'json';
        $json = $this->BookmarkCandidate->change($this->request, Auth::userID());
        $this->set(compact('json'));
    }
    /**
     * 担当の一時変更
     */
    public function reserve()
    {
        $this->layout = 'json';
        $json = $this->BookmarkCandidate->reserve(
            $this->request, Auth::userID()
        );
        $this->set(compact('json'));
    }
    /**
     * ブックマークレット生成
     */
    public function bookmarklet()
    {
        $domain = getEnv('HTTP_HOST');
        $prot = (getEnv('SERVER_PORT') == 80) ? 'http' : 'https';

        echo <<<BM
javascript:location="{$prot}://{$domain}TPF_BASE_URL/candidates/regist?url="+encodeURI(window.location)+"&title="+eval("encodeURI(document.title.substr(0,100))")+"&summary="+eval("encodeURI(document.getElementsByName('description').item(0).content.substr(0,600))")
BM;
        exit;
    }
}
