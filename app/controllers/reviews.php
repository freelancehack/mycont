<?php

class ReviewsController extends AppWebController
{
    //public $useModels = ['Review', 'User'];
    public $useModels = ['Bookmark' => ['Review'], 'User'];

    public $requestParams = [
        'index' => [
            'id',
            'review_status' => ['default' => 'pending'],
            'user_id',
            'page'  => ['min' => 1],
            'sort' => ['default' => ['modified' => 'ASC']]
        ],
        'change_status' =>[
            'id',
            'to_status',
            'comment'
        ],
        'repending' =>[
            'id',
            'to_status',
            'category_id',
            'title',
            'url',
            'summary',
            'wp_post_id',
            'noncapture',
            'comment'
        ]
    ];

    public function index()
    {
        $targetUserIds = null;
        if (Auth::isPartner()) {
            $this->request['user_id'] = Auth::userID();
        } elseif (Auth::isEditor() && !Auth::isAdmin() && empty($this->request['user_id'])) {
            $targetUserIds = $this->request['user_id'] = array_merge($this->User->getPartnerIds(), [Auth::userID()]);
        }
        if (!empty($this->request['id'])) {
            $this->request = [
                'id' => $this->request['id'],
                'review_status' => '',
                'user_id' => '',
                'page' => '1',
                'sort' => '',
            ];
        }
        list($reviews, $pagination) = $this->BookmarkReview->onReviews($this->request, $targetUserIds);
        $request = $this->request;
        $selectElement = 'review';
        $partnerUsers = $this->BookmarkReview->partnerUser($targetUserIds);
        $this->set(compact('reviews', 'partnerUsers', 'pagination', 'request', 'selectElement'));
    }
    public function changeStatus()
    {
        $this->layout = 'json';
        $json = $this->BookmarkReview->change($this->request, Auth::userID());
        $this->set(compact('json'));
    }

        public function repending()
    {
        $this->layout = 'json';
        $json = $this->BookmarkReview->change($this->request, Auth::userID());
        $this->set(compact('json'));
    }
}
