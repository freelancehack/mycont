<?php

class DefaultController extends AppWebController
{
    public $useModels = ['Status'];

    public function index()
    {
        $createdTransition = $this->Status->crawlingStatus();
        $barChart2 = $this->Status->workStatus();
        $barChart3 = $this->Status->outdatedNewsStatus();
        $this->set(compact('createdTransition', 'barChart2', 'barChart3'));
        
    }
}
