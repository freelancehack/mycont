<?php

class BookmarkInfoController extends AppWebController
{
    public $useModels = ['Bookmark' => ['Info', 'Review']];

    public $requestParams = [
        'index' => [
            'id',
        ]
    ];

    public function index()
    {
        list($fhBookmark, $wpPost, $histories) = $this->BookmarkInfo->get($this->request);
        $this->set(compact('fhBookmark', 'wpPost', 'histories'));
    }

}
