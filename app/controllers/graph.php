<?php

class GraphController extends AppWebController
{
    public $useModels = ['Status'];

    public $requestParams = [
        'index' => [
            'graph_type',
            'end_date' 
        ],
    ];
    public function index()
    {
    	$graphType = $this->request['graph_type'];
        $endDate = $this->request['end_date'];
        if (empty($endDate)) {
            $endDate = current_time('Y-m-d');
            $date = new DateTime($endDate);
            $date->sub(new DateInterval("P32D"));
            $endDate =  $date->format('Y-m-d');
        }
        $date = new DateTime($endDate);
        $date->sub(new DateInterval("P32D"));
        $previousEndDate =  $date->format('Y-m-d');

    	$data = $this->Status->$graphType($endDate);

        $this->set(compact('data', 'graphType', 'previousEndDate'));
        
    }

}
