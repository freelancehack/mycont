<?php

class MypageController extends AppWebController
{
    public $useModels = ['User', 'Bookmark' => ['Candidate']];

    public $requestParams = [
        'index' => [
            'target_month',
        ],
        'work_histories' => [
            'page' => ['default' => 1],
            'target_month',
            'user_id',
        ]
    ];

    public function index()
    {
        if (!$this->request['target_month']) {
            $this->request['target_month'] = date_i18n('Y-m');
        }
        $this->request['page'] = 1;

    	if (Auth::isAdmin()) {
	        list($myData, $workHistoryMonths) = $this->User->getStatusAllUser($this->request);
    	} else {
            $userId = Auth::userID();
	        list($myData[$userId], $workHistoryMonths) = $this->User->getStatusByUserId($userId, $this->request);
    	}
        $countRecommneded = $this->BookmarkCandidate->countRecommneded();
        $this->set(compact('myData', 'workHistoryMonths', 'countRecommneded'));
    }

    public function workHistories()
    {
        if (!$this->request['target_month']) {
            $this->request['target_month'] = date_i18n('Y-m');
        }
        $this->set(['targetMonth' => $this->request['target_month']]);

        if (Auth::isAdmin()) {
            if (empty($this->request['user_id'])) {
                $this->request['user_id'] = Auth::userID();
            }
            $user_id = $this->request['user_id'];
        } else {
            $user_id = Auth::userID();
            $this->request['user_id'] = null;
        }
        $this->set(['targetMonth' => $this->request['target_month']]);

        list($histories, $pagination, $selectForm) = $this->User->myWorkHistories($this->request, $user_id);
        $this->set(compact('histories', 'pagination', 'selectForm'));
    }
}
