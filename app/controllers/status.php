<?php

class StatusController extends AppWebController
{
    public $useModels = ['Status', 'SourceDomain'];

    public $requestParams = [
         'disabled_domains' =>[
            'domain',
            'review_status' => ['default' => 'disabled'],
            'reject_source' => ['default' => 0],
        ],
         'update_disabled_domain' =>[
            'id',
            'reject_source',
        ],
    ];

    public function disabledDomains()
    {
        $request = $this->request;
        $domains = $this->Status->disabledDomains($this->request);
        $this->set(compact('domains', 'request'));
    }

    public function updateDisabledDomain()
    {
        $this->layout = 'json';
        $json = $this->SourceDomain->update($this->request, Auth::userId());
        $this->set(compact('json'));   	
    }
}
