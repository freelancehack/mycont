-- MySQL dump 10.13  Distrib 5.6.28, for Linux (x86_64)
--
-- Host: localhost    Database: fh-wp
-- ------------------------------------------------------
-- Server version	5.6.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fh_partner_fees`
--

DROP TABLE IF EXISTS `fh_partner_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fh_partner_fees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `fh_task_id` int(11) NOT NULL,
  `fee` int(11) NOT NULL,
  `effective_date` date NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='報酬マスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fh_partner_fees`
--

LOCK TABLES `fh_partner_fees` WRITE;
/*!40000 ALTER TABLE `fh_partner_fees` DISABLE KEYS */;
INSERT INTO `fh_partner_fees` VALUES (1,0,1,20,'2016-01-01','2016-07-15 18:22:26'),(2,0,2,80,'2016-01-01','2016-07-15 18:22:26'),(3,0,3,20,'2016-01-01','2016-07-15 18:22:26'),(4,0,4,170,'2016-01-01','2016-07-15 18:22:26'),(5,0,5,20,'2016-01-01','2016-07-15 18:22:26'),(6,0,6,20,'2016-01-01','2016-07-30 06:15:41'),(7,0,7,250,'2016-01-01','2016-07-30 06:15:41'),(8,0,8,-40,'2016-01-01','2016-07-30 06:17:08'),(9,0,9,10,'2016-01-01','2016-07-30 06:17:08'),(10,0,10,20,'2016-01-01','2016-07-30 06:17:08'),(11,0,11,20,'2016-01-01','2016-07-30 06:17:08'),(12,0,12,100,'2016-01-01','2016-07-30 06:17:08'),(13,0,13,10,'2016-01-01','2016-07-30 06:17:08'),(14,0,14,10,'2016-01-01','2016-07-30 06:17:08'),(15,0,15,100,'2016-01-01','2016-07-31 02:18:26'),(16,0,16,-10,'2016-01-01','2016-07-31 02:18:26'),(17,0,20,10,'2016-01-01','2016-08-31 18:59:00'),(18,0,21,50,'2016-01-01','2016-08-31 18:59:00'),(19,0,22,50,'2016-01-01','2016-08-31 18:59:00');
/*!40000 ALTER TABLE `fh_partner_fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fh_tasks`
--

DROP TABLE IF EXISTS `fh_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fh_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `editor` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fh_tasks`
--

LOCK TABLES `fh_tasks` WRITE;
/*!40000 ALTER TABLE `fh_tasks` DISABLE KEYS */;
INSERT INTO `fh_tasks` VALUES (1,21,'News 候補の削除',0,'2016-07-30 06:12:27'),(2,22,'News レビュー依頼',0,'2016-07-30 06:12:27'),(3,23,'News レビューの公開',1,'2016-07-30 06:12:27'),(4,24,'News レビューの被公開',0,'2016-07-30 06:12:27'),(5,25,'News レビューの差し戻し',1,'2016-07-30 06:12:27'),(6,26,'News 再レビュー',0,'2016-07-30 06:12:27'),(7,27,'News 直接公開',1,'2016-07-30 06:12:27'),(8,28,'News レビューの却下削除',0,'2016-07-31 02:16:30'),(9,31,'ブックマーク 候補の削除',0,'2016-08-06 04:46:56'),(10,32,'ブックマーク レビュー依頼',0,'2016-08-06 04:46:56'),(11,33,'ブックマーク レビューの公開',1,'2016-08-06 04:46:56'),(12,34,'ブックマーク レビューの被公開',0,'2016-08-06 04:46:56'),(13,35,'ブックマーク レビューの差し戻し',1,'2016-08-06 04:46:56'),(14,36,'ブックマーク 再レビュー',0,'2016-08-06 04:46:56'),(15,37,'ブックマーク 直接公開',1,'2016-08-06 04:46:56'),(16,38,'ブックマーク レビューの却下削除',0,'2016-08-06 04:46:56'),(17,81,'週落ちnewsのremove',1,'2016-08-13 20:25:05'),(18,71,'担当予約',0,'2016-08-14 20:06:45'),(19,82,'担当予約の解除',1,'2016-08-14 20:07:20'),(20,1,'重要記事 単価アップ レビューの公開',1,'2016-08-31 18:55:11'),(21,2,'重要記事 単価アップ レビューの被公開',0,'2016-08-31 18:55:11'),(22,3,'重要記事 単価アップ  直接公開',1,'2016-08-31 18:55:11'),(23,72,'判断見送り',0,'2016-08-31 18:55:11'),(24,73,'重要記事としてマーキング',1,'2016-08-31 18:55:11'),(25,83,'ドメイン拒否によるdisabled',1,'2016-09-19 17:01:53');
/*!40000 ALTER TABLE `fh_tasks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-24 18:49:04
