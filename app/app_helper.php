<?php
class AppHelper
{
    private $lineGraph = null;
    private $barChart = null;
    private $barChart2 = null;

    /*
     * 折れ線グラフ
     */
    public function setLineGraphParam($data)
    {
        // ticks
        $ticks = array();
        foreach ($data['data']['labels'] as $i => $val) {
            $ticks[] = "[{$i},'{$val}']";
        }
        // data
        $datas = array();
        foreach ($data['data']['values'][0] as $i => $val) {
            $datas[] = "[{$i},{$val}]";
        }
        $this->lineGraph = array(
            'min' => $data['min'],
            'max' =>  $data['max'],
            'tickInterval' => $data['step'],
            'ticks' => '['. implode(',', $ticks). ']',
            'data' => '[['. implode(',', $datas). ']]'
        );
    }
    /*
     * 棒グラフ
     */
    public function setBarChartParam($data)
    {
        // ticks
        $ticks = array();
        foreach ($data['data']['labels'] as $i => $val) {
            $ticks[] = "[{$i},'{$val}']";
        }
        // data
        $datas = array();
        foreach ($data['data']['values'][0] as $i => $val) {
            $datas[] = "[{$i},{$val}]";
        }
        $this->barChart = array(
            'min' => $data['min'],
            'max' =>  $data['max'],
            'tickInterval' => $data['step'],
            'ticks' => '['. implode(',', $ticks). ']',
            'data' => '[['. implode(',', $datas). ']]',
            'barWidth' => 30
        );
    }


    public function setBarChart2Param($barData, $barTicks)
    {
        $str = [];
        foreach ($barData as $name => $data) {
            $strs = [];
            foreach ($data as $index => $count) {
                $strs[] = "[{$index}, {$count}]";

            }
            $str[] = '[ '. implode(', ', $strs). ' ]';
            $seriesStrs[] = "{ label :'{$name}' }";
        }
        $varStr = implode(",\n", $str);

        $ticksStrs = [];
        foreach ($barTicks as $index => $label) {
            $ticksStrs[] = "[{$index}, '{$label}']";
        }

        $ticksStr = '[ '. implode(', ', $ticksStrs). ' ]';
        $seriesStr = '[ '. implode(', ', $seriesStrs). ' ]';

        $this->barChart2 = array(
            'data' => '['. implode(",\n", $str). ']',
            'ticks' => '[ '. implode(', ', $ticksStrs). ' ]',
            'series' => '[ '. implode(', ', $seriesStrs). ' ]'
        );
    }

    public function setBarChart3Param($barData, $barTicks)
    {
        $str = [];
        foreach ($barData as $name => $data) {
            $strs = [];
            foreach ($data as $index => $count) {
                $strs[] = "[{$index}, {$count}]";

            }
            $str[] = '[ '. implode(', ', $strs). ' ]';
            $seriesStrs[] = "{ label :'{$name}' }";
        }
        $varStr = implode(",\n", $str);

        $ticksStrs = [];
        foreach ($barTicks as $index => $label) {
            $ticksStrs[] = "[{$index}, '{$label}']";
        }

        $ticksStr = '[ '. implode(', ', $ticksStrs). ' ]';
        $seriesStr = '[ '. implode(', ', $seriesStrs). ' ]';

        $this->barChart3 = array(
            'data' => '['. implode(",\n", $str). ']',
            'ticks' => '[ '. implode(', ', $ticksStrs). ' ]',
            'series' => '[ '. implode(', ', $seriesStrs). ' ]'
        );
    }

    /*
     * myGlobalVars.jqplot の描画
     */
    public function render()
    {
        $lineGraphVars = '';
        $barChartVars = '';
        $barChart2Vars = '';
        $barChart3Vars = '';
        if ($this->lineGraph) {
            foreach ($this->lineGraph as $name => $val) {
                $lineGraphVars .= "{$name}: {$val},\n";
            }
        }
        if ($this->barChart) {
            foreach ($this->barChart as $name => $val) {
                $barChartVars .= "{$name}: {$val},\n";
            }
        }
        if ($this->barChart2) {
            foreach ($this->barChart2 as $name => $val) {
                $barChart2Vars .= "{$name}: {$val},\n";
            }
        }
        if ($this->barChart3) {
            foreach ($this->barChart3 as $name => $val) {
                $barChart3Vars .= "{$name}: {$val},\n";
            }
        }

        echo <<<HTML
<script>
  var myGlobalVars = myGlobalVars || {};
  myGlobalVars.jqplot= {
    lineGraph : {
$lineGraphVars
    },
    barChart : {
$barChartVars
    },
    barChart2 : {
$barChart2Vars
    },
    barChart3 : {
$barChart3Vars
    },
  }

</script>
HTML;
    }
}
