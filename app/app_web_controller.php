<?php

class AppWebController extends WebController
{
    public $request = [];

    public $view = "AppView";

    public $configure = [];

    public function __construct($controller)
    {
        parent::__construct($controller);
        Auth::check();
        $this->setRequest();
        $this->autoLinks = $this->getAutolinkFileConfigure();
    }

    protected function setRequest()
    {
        if (is_array($this->requestParams[$this->action])) {
            foreach ($this->requestParams[$this->action] as $name => $value) {
                if (is_numeric($name)) {
                    $name = $value;
                    $value = null;
                }
                $this->request[$name] = isset($_GET[$name]) ? $_GET[$name] : @$_POST[$name];
                if ($this->request[$name] === null) {
                    $this->request[$name] = (!empty($value['default'])) ? $value['default'] : null;
                }
                if (!empty($value['min'])) {
                    $this->request[$name] = ((int)$this->request[$name] < (int)$value['min']) ? (int)$value['min'] : (int)$this->request[$name];
                }
                if (!empty($value['func'])) {
                    //$this->request[$name] = @${$value['func']}($this->request[$name]);
                }
            }
        }
    }

    protected function beforeFilter()
    {
        $this->setViewParams();
    }

    protected function setViewParams()
    {
        $isAdmin = Auth::isAdmin();
        $isEditor = Auth::isEditor();
        $isPartner = Auth::isPartner();
        $userData = Auth::userData();
        $scdomain = $this->getScreenShotServerDomain();
        $request = $this->request;
        $this->set(
            compact(
                'isAdmin',
                'isEditor', 
                'isPartner', 
                'userData', 
                'scdomain',
                'request'
                ));
    }

    protected function isProd()
    {
        return getEnv('HTTP_HOST') === 'freelance-hack.com';
    }
    protected function getScreenShotServerDomain()
    {
        return $this->isProd() ? 'https://fh.odcdn.tokyo/' : 'http://screenshot.net/';
    }
    protected function getAutolinkFileConfigure()
    {
        $configure = new AppConfigure();
        return $configure->autoLinkFiles();
    }

}
