<a href="?review_status=disabled">disabled</a> |
<a href="?review_status=null">null</a> |
<a href="?reject_source=0,1">both</a> |



<table>
	<tr>
		<th><?php echo $request['review_status']; ?></th>
		<th>domain</th>
		<th>reject</th>
		<th>publish</th>
		<th>Accuracy</th>
	</tr>


<?php




foreach ($domains as $value) {
$cnt = (int)$value['cnt'];
$pcnt = (int)$value['pcnt'];
$total = $cnt + $pcnt;
$domain = $value['domain'];
$accuracy = floor($cnt / $total * 100);

$baseUrl = TPF_BASE_URL;

$checked = $value['reject_source'] ? 'checked' : '' ;

echo <<<EOD
	<tr>
		<td><a href="{$baseUrl}/raws?domain={$domain}&review_status=disabled">{$cnt}</a></td>
		<td>{$value['domain']} <a href="http://{$value['domain']}" target="_blank">□</a></td>
		<td>
			<form>
				<input type="radio" name="reject_source" value="0"id="rejectOff{$value['fh_source_domain_id']}"
					checked
				>
				<label for="rejectOff{$value['fh_source_domain_id']}">自動収集</label>
				<input type="radio" name="reject_source" value="1" id="rejectOn{$value['fh_source_domain_id']}"
					{$checked}
				>
				<label for="rejectOn{$value['fh_source_domain_id']}">拒否</label>
				<input type="hidden" name="id" value="{$value['fh_source_domain_id']}">
			</form>
		</td>
		<td><a href="{$baseUrl}/raws?domain={$domain}&review_status=publish">{$pcnt}</a></td>
		<td>{$accuracy}</td>
	</tr>
EOD;

}

?>

</table>