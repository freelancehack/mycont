<?php
if (empty($candidates)) {
    return;
}
echo $this->element('components/paginate');
?>

    <div id="content">
        <?php echo $this->element('pages/contributes/urls'); ?>
    </div>
    <div id="form-tpl">
        <form action="<?php echo TPF_BASE_URL; ?>/contributes/" id="form{index}"
            data-endisabled-url="<?php echo TPF_BASE_URL; ?>/candidates/change?id={id}&column=review_status&value=disabled">
            <input type="hidden" name="id" value="{id}"  id="id{index}">
            <table class="form">
                <tr>
                    <th rowspan="4" class="index">{id}</th>
                    <td colspan="2"><a href="" class="open_url" target="_blank">紹介URL:</a><br>
                        <input class="capture" type="text" name="url"></td>
                    <th rowspan="4">
                        <input type="button" value="確認" class="confirm" data-form-name="form{index}">
                    </th>
                </tr>
                <tr>
                    <td>
                        タイトル:<br>
                        <textarea name="title" id="title_{index}" rows="5"></textarea><br>
                        {publish_date}<br>
                        <input type="checkbox" name="noncapture" value="1"  id="noncapture{index}">
                        <label for="noncapture{index}">キャプチャ非掲載</label><br>
                        <input type="checkbox" name="category_id[]" value="44" id="cid44_{index}" checked>
                        <label for="cid44_{index}">ブックマーク</label><br>
                        <input type="checkbox" name="category_id[]" value="49" id="cid49_{index}">
                        <label for="cid49_{index}">News</label><br>
                        <p class="summary"></p>
                    </td>
                    <td class="summary">要約:<br><textarea name="summary" id="summary_{index}" rows="10"></textarea></td>
                </tr>
            </table>
            <div class="preview"></div>
            <div class="buttons">
                <?php if ($isEditor) : ?>
                <input type="button" value="削除" class="disabled" data-form-name="form{index}">
                <span class="disabled"> | </span>             
                <input type="button" value="投稿" class="submit" data-form-name="form{index}"> | 
                <?php endif; ?>
                <input type="button" value="レビュー依頼" class="pennding" data-form-name="form{index}">
            </div>
        </form>
    </div>
            <div id="postmain-tpl">
                <!-- [ #container ] -->
                <div class="innerBox" id="container">
                    <!-- [ #content ] -->
                    <div class="content" id="content">
                    <!-- [ #post- ] -->
                        <h1 class="entryPostTitle entry-title">{title}</h1>
                        <div class="entry-content post-content">
                            <div class="center capture">
                                <a href="{url}" target="_blank">
                                    <img src="" data-url="<?php echo $scdomain; ?>capture/{url}/320x240/square.png&preview=true">
                                </a>
                                <p>スクリーンショット {url}</p>
                            </div>
                            <blockquote>
                                <ul>
                                </ul>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>

    <a rel="leanModal" href="#leanModal"></a>
    <div id="leanModal"><button class="modal_close">更新</button><br><textarea id="leanModalTextarea" data-id=""></textarea></div>
 
<?php
echo $this->element('components/paginate');
