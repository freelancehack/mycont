<?php

$notice = ($request['category_id'] == 49) ? '1週間以内に収集された候補が日付の登録順に表示されます' : '収集日の新しい順に表示されています';

echo <<<EOD
<div class="notice">
{$notice}
</div>


EOD;
if (empty($candidates)) {
	return;
}
echo $this->element('components/paginate');
echo $this->element('pages/candidates/table');
echo $this->element('components/paginate');
