<?php
	switch ($graphType) {
	    case 'crawlingStatus':
		   	$this->AppHelper->setLineGraphParam($data);
		   	$title = 'クローラー収集推移グラフ';
		   	$graphId = 'crawler-regist-graph';

			break;
	    case 'workStatus':
		   	$this->AppHelper->setBarChart2Param($data['data'], $data['ticks']);
		   	$title = 'レビューステータス';
		   	$graphId = 'review-status-gragh';
	        break;
	    case 'outdatedNewsStatus':
		   	$this->AppHelper->setBarChart3Param($data['data'], $data['ticks']);
		   	$title = 'ニュース落ち数';
		   	$graphId = 'outdated-news-gragh';

	        break;
	}
?>
<h2><?php echo $title; ?></h2>

<div style="width: 100%;background-color: #F9F9F9;border: 1px solid #efefef;">
    <div id="<?php echo $graphId; ?>"  style="width: 95%;height: 220px;margin: auto;"></div>
</div>
<div class="previous_graph">
    <a href="<?php echo TPF_BASE_URL. "/graph?graph_type={$graphType}&end_date={$previousEndDate}"; ?>">
        前へ
    </a>
</div>
<?php
	
    echo $this->AppHelper->render();
