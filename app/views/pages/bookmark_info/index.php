<?php

$row = $fhBookmark[0];
$summaryCreated = substr($row['created'], 0, 10);
$urlTxt = substr($row['url'], 0, 35). "...";

$p = $wpPost[0];

$FhReview = new BookmarkReview();
$reviewStatuses = $FhReview->reviewStatuses;
    echo <<<EOD
<h2>{$reviewStatuses[$row['review_status']]}</h2>
<table>
	<caption>収集データ</caption>
	<tr>
    	<td rowspan="4">
            {$row['id']}
        </td>
        <td>
            {$row['publish_date']}
            ({$summaryCreated})
            {$row['domain']}
            担当: {$row['wp_users_display_name']}
            {$row['reserved_expire']}
        </td>
    </tr>
    <tr>
        <td>{$row['title']}</td>
    </tr>
    <tr>
        <td><a href="{$row['url']}" target="_blank">$urlTxt</a></td>
    </tr>
    <tr>
        <td>{$row['summary']}</td>
    </tr>
</table>
EOD;

    echo <<<EOD
<table>
	<caption>WP POST</caption>
	<tr>
		<td><a href="/wp-admin/post.php?post={$p['ID']}&action=edit">{$p['post_title']}</a></td>
    </tr>
	<tr>
		<td>{$p['post_content']}</td>
    </tr>
</table>
EOD;



	echo $this->element(
		'components/history_table',
		[
			'caption' => '作業履歴',
		]
		);

?>
<a href="javascript:history.back();">戻る</a>