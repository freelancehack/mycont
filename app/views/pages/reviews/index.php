<?php
if (empty($reviews)) {
	return;
}

echo $this->element('components/paginate');

$FhReview = new BookmarkReview();
$reviewStatuses = $FhReview->reviewStatuses;

$tbu = TPF_BASE_URL;

foreach($reviews as $id => $review) {
	$reviewStatus = $review['FhBookmark']['review_status'];
	$displayName = $review['FhBookmark']['display_name'];

	$operationButtons = $this->element('pages/reviews/operation_buttons', compact('reviewStatus', 'isEditor'));

	$comment = null;
	$commentTr = null;
	if(!empty($review['FhBookmarkHistory'][0]['params'])) {
		$paramsSerialized = $review['FhBookmarkHistory'][0]['params'];
		$params = unserialize($paramsSerialized);
		if (!empty($params['request']['comment'])) {
			$comment = $review['FhBookmarkHistory'][0]['wp_users_display_name']. 'さんからのコメント:<br>';
			$comment .= htmlspecialchars($params['request']['comment']);
		}
	}
	if ($comment) {
		$commentTr = <<<EOD
			<tr>
				<td colspan="3">{$comment}</td>
			</tr>

EOD;

	}


	echo <<<EOD
<div class="review_block"  id="review_{$id}">
	<form action="{$tbu}/reviews/" method="POST">
		<input type="hidden" value="" name="to_status" id="to_status_{$id}">
		<input type="hidden" value="{$id}" name="id">
		<input type="hidden" value="{$review['WpPost']['ID']}" name="wp_post_id">
		<table>
			<tr>
				<th>id</th>
				<th>ステータス</th>
				<th>担当者</th>
			</tr>
			<tr>
				<td>{$id}</td>
				<td>{$reviewStatuses[$reviewStatus]}</td>
				<td>{$displayName}</td>
			</tr>
			{$commentTr}
		</table>
EOD;

	if ($reviewStatus === 'remand') {
		echo $this->element('pages/reviews/form', compact('review', 'id'));		
	} else {
		echo $this->element('pages/reviews/view', compact('review'));		
	}


	$commentArea = <<<EOD
		<div class="comment_area">コメント追加
			<textarea name="comment"></textarea>
		</div>
EOD;

	if ($isPartner) {
		if ($reviewStatus != 'remand') {
			$commentArea = null;
		}

	}

	echo <<<EOD
		{$commentArea}
		<div class="operation_buttons">
		{$operationButtons}
		</div>
	</form>
	<table class="histories">
EOD;
	if(count($review['FhBookmarkHistory'])) foreach ($review['FhBookmarkHistory'] as $history) {
	echo <<<EOD
		<tr>
			<th>{$history['id']}</th>
			<td>{$history['fh_tasks_name']}</td>
			<td>{$history['wp_users_display_name']}</td>
			<td>{$history['created']}</td>
		</tr>
EOD;
	}
	echo <<<EOD
	</table>
EOD;
?>
</div>
<?php
}

echo $this->element('components/paginate');
?>

