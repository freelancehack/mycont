
<h1>
ようこそ <?php echo $userData['display_name']; ?> さん
</h1>

<?php
foreach ($myData as $userId => $v) {
	$fee = $v['fee'];
	$status = $v['status'];
?>
<table class="my_status_table">
	<caption><?php echo $fee[0]['display_name']; ?> さんのステータス</caption>
	<tr>
		<th>担当</th>
		<th>推薦</th>
		<th>差し戻し</th>
		<th>レビュー待ち</th>
		<th>再レビュー待ち</th>
	</tr>
	<tr>
		<td class="number">
			<a href="<?php
						echo TPF_BASE_URL.
							'/candidates/?&crawler_id=&category_id=&user_id='.
							$fee[0]['user_id'];
						?>">
				<?php echo $status['owns']; ?>
			</a>
		</td>
		<td class="number">
			<a href="<?php
				echo TPF_BASE_URL;
			?>/candidates/?&crawler_id=&category_id=&recommended=1">
			<?php echo $countRecommneded; ?>
			</a>
		</td>
		<td class="number">
			<a href="<?php
				echo TPF_BASE_URL.
					'/reviews/?&review_status=remand';
				?>">
				<?php echo $status['remand']; ?>
			</a>
		</td>
		<td class="number">
			<a href="<?php
				echo TPF_BASE_URL.
					'/reviews/';
				?>">
				<?php echo $status['pending']; ?>
			</a>
		</td>
		</td>
		<td class="number">
			<a href="<?php
				echo TPF_BASE_URL.
					'/reviews/?&review_status=repending';
				?>">
				<?php echo $status['repending']; ?>
			</a>
		</td>
	</tr>
	<tr>
		<th colspan="5">担当自動解除数(今月)</th>
	</tr>
	<tr>
		<td colspan="5" class="number"><?php echo $status['reserved_canceled']; ?></th>
	</tr>
</table>



<table class="fee_table">
	<caption>
		<?php echo $fee[0]['display_name']; ?> さんの報酬
		<?php echo $this->element('pages/mypage/target_month_form', ['userId' => $userId]); ?>
	</caption>
	<tr>
		<th>作業内容</th>
		<th>回 数</th>
		<th>単 価</th>
		<th>作業報酬額</th>
	</tr>
<?php
$monthTotal = 0;
foreach ($fee as $history) {
	if ($history['editor'] && Auth::isPartner()) {
		continue;
	}
	$trClass = ($history['fee'] < 0) ? 'minus_fee' : '';

	$monthTotal = $monthTotal + (int)$history['total'];
	echo <<<EOD
<tr class="{$trClass}">
	<td>{$history['name']}</td>
	<td class="number">{$history['cnt']}</td>
	<td class="number">{$history['fee']} 円</td>
	<td class="number">{$history['total']} 円</td>
</tr>
EOD;
}
?>
	<tr>
	<td colspan="4" class="number">  <?php echo $request['target_month']; ?> の合計報酬額 <?php echo number_format($monthTotal); ?>円</td>
	</tr>
</table>

<?php
	echo $this->element(
		'components/history_table',
		[
			'caption' => $fee[0]['display_name']. 'さんの最近の作業履歴',
			'histories' => $v['workHistory'],
		]
		);
	echo <<<EOD

<a href="work_histories?user_id={$fee[0]['user_id']}&target_month={$request['target_month']}">もっと履歴を見る</a>
EOD;
}
