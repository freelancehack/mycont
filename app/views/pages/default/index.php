
<h2>クローラー収集推移グラフ</h2>

<div style="width: 100%;background-color: #F9F9F9;border: 1px solid #efefef;">
    <div id="crawler-regist-graph"  style="width: 95%;height: 220px;margin: auto;"></div>
</div>

<div class="previous_graph">
	<a href="<?php echo TPF_BASE_URL. '/graph?graph_type=crawlingStatus'; ?>">
		前へ
	</a>
</div>

<h2>レビューステータス</h2>
<div style="width: 100%;background-color: #F9F9F9;border: 1px solid #efefef;">
    <div id="review-status-gragh"  style="width: 95%;height: 220px;margin: auto;"></div>
</div>
<div class="previous_graph">
    <a href="<?php echo TPF_BASE_URL. '/graph?graph_type=workStatus'; ?>">
        前へ
    </a>
</div>

<h2>ニュース落ち数</h2>
<div style="width: 100%;background-color: #F9F9F9;border: 1px solid #efefef;">
    <div id="outdated-news-gragh"  style="width: 95%;height: 220px;margin: auto;"></div>
</div>
<div class="previous_graph">
    <a href="<?php echo TPF_BASE_URL. '/graph?graph_type=outdatedNewsStatus'; ?>">
        前へ
    </a>
</div>


<?php

    echo $this->AppHelper->setLineGraphParam($createdTransition);
    echo $this->AppHelper->setBarChart2Param($barChart2['data'], $barChart2['ticks']);
    echo $this->AppHelper->setBarChart3Param($barChart3['data'], $barChart3['ticks']);
    echo $this->AppHelper->render();
