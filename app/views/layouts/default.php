<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="referrer" content="no-referrer">
    <title><?php echo $pageTitle; ?></title>
    <?php echo $this->stylesheets($pageTpl); ?>
</head>
<body>
    <header role="banner">
        <nav role="navigation" class="gnavi">
            <button class="slide_btn btn">Menu</button>
            <div class="slide_box toggle_box">

            <ul>
                <li><a href="<?php echo TPF_BASE_URL; ?>/">TOP</a></li>
                <li><a href="<?php echo TPF_BASE_URL; ?>/candidates/">候補一覧</a></li>
                <li><a href="<?php echo TPF_BASE_URL; ?>/contributes/">投稿</a></li>
                <li><a href="<?php echo TPF_BASE_URL; ?>/reviews/">レビュー中</a></li>
                <li><a href="<?php echo TPF_BASE_URL; ?>/mypage/">マイページ</a></li>
                <?php if($isAdmin): ?>
                <li><a href="<?php echo TPF_BASE_URL; ?>/candidates/bookmarklet">ブックマークレット</a></li>
                <li><a href="<?php echo TPF_BASE_URL; ?>/status/disabled_domains">ドメイン解析</a></li>
                <li><a href="/wp-login.php?action=logout">ログアウト</a></li>

                <?php endif; ?>
            </ul>
            </div>
            <!--?php echo $globalNavi; ?-->
        </nav>
        <a href="/" class="head_image">
        <img src="/wp-content/uploads/2015/05/festisite_emirates.gif" alt="フリーランスハック (Freelance Hack)">
        </a>
        <h1><?php echo $pageTitle; ?></h1>
        <div class="lnavi">
            <?php if ($request && $selectElement) echo $this->element('components/select_form_'. $selectElement); ?>
        </div>
    </header>
    <main role="main">
    <?php echo $content; ?>
    </main>
    <footer role="contentinfo">
        © Otona Design inc.
    </footer>

    <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
    <?php echo $this->scripts($pageTpl); ?>

</body>
</html>