<table class="bookmarks">
	<tr>
    	<th rowspan="4">id</th>
        <th>記事公開日(収集日)</th>
        <th rowspan="4">操作</th>
    </tr>
    <tr>
        <th>記事タイトル</th>
    </tr>
    <tr>
        <th>記事URL</th>
    </tr>
    <tr>
        <th>記事概要</th>
    </tr>
    <?php
        $cate = array(44 => 'ブックマーク', 49 => 'news');
    foreach ($candidates as $row) {
        $summaryCreated = substr($row['created'], 0, 10);
        $urlTxt = substr($row['url'], 0, 35). "...";
        $to_cate =  ($row['category_id'] == 44)? 49 : 44;
        $deleteBtn = <<<EOD
            <button
                data-href="change?&id={$row['id']}&column=review_status&value=disabled"
                data-id="{$row['id']}"
                data-message="候補から削除しました">
                削除</button>
EOD;

        $noticePreviousNewsFlg = ($row['category_id'] == 49 
            && (strtotime($row['publish_date']) < strtotime('-12 days 9 hours') ||  strtotime($row['created']) <= strtotime('-4 days 9 hours'))
            );
        $previousNewsClass = $noticePreviousNewsFlg ? 'notice_previous_news' : '';
        $previousNewsFlg = ($row['category_id'] == 49 
            && (strtotime($row['publish_date']) < strtotime('-14 days 9 hours') ||  strtotime($row['created']) <= strtotime('-6 days 9 hours'))
            );
        $previousNewsClass = $previousNewsFlg ? 'previous_news' : $previousNewsClass;
        $previousNewsMsg = $previousNewsFlg ? '<br>本日' : '';
        $changeableNewsFlg = (strtotime($row['publish_date']) < strtotime('-7 days 9 hours') && $row['category_id'] == 49 );
        if($isAdmin
            || $changeableNewsFlg) {
            $changeBtn = <<<EOD
            <button 
                data-href="change?id={$row['id']}&column=category_id&value={$to_cate}"
                data-id="{$row['id']}"
                data-message="カテゴリを{$cate[$to_cate]}に変更しました"
                >変更</button>
EOD;


        }
        // adminの場合
        $recommendedTxt = '<span class="recommended_mark">推</span>';
        if ($isAdmin) {
            $passBtn = null;
            $recommendedBtn = $row['recommended'] ? $recommendedTxt : <<<EOD
            <button
                class="recommended"
                data-href="change?id={$row['id']}&column=recommended&value=1"
                data-id="{$row['id']}"
                data-message="候補を推薦しました"
                >推薦</button>
EOD;
        } else {
            $passBtn = <<<EOD
            <button 
                data-href="pass?id={$row['id']}"
                data-id="{$row['id']}"
                data-message="候補をパスしました"
                >パス</button>
EOD;
            $recommendedBtn = $row['recommended'] ? $recommendedTxt : null;
            $passBtn = $row['recommended'] ? null : $passBtn;

        }
        // 自分の担当の場合
        if ($row['user_id'] != $userData['ID']) {
            $reserveBtn = <<<EOD
            <button
                class="reserve"
                data-href="reserve?id={$row['id']}"
                data-id="{$row['id']}"
                data-display_name="{$userData['display_name']}"
                data-message="担当を自分に変更しました"
                >担当</button>
EOD;

        } else {
            $passBtn = null;
            $deleteBtn = null;
            $changeBtn = null;
            $reserveBtn = null;
            $previousNewsClass = 'is_owner';

        }

        $domainStr = $isAdmin ? 
            '<a href="'. TPF_BASE_URL.'/raws?domain='. $row['domain']. '">'.$row['domain'].'</a>'
            : $row['domain'];

        echo <<<EOD
	<tr id="id{$row['id']}">
    	<td rowspan="4" class="{$previousNewsClass}">
            <a name="id{$row['id']}">{$row['id']}</a>
            {$previousNewsMsg}
            {$recommendedBtn}
            {$passBtn}
        </td>
        <td>
            {$row['publish_date']}
            ({$summaryCreated})
            {$domainStr}
            担当: <span class="display_name_{$row['id']}">{$row['display_name']}</span>
            {$row['reserved_expire']}
        </td>
        <td rowspan="4">
                {$deleteBtn}
                {$changeBtn}
                {$reserveBtn}
        </td>
    </tr>
    <tr class="row{$row['id']}">
        <td><a href="../contributes?id={$row['id']}">{$row['title']}</a></td>
    </tr>
    <tr class="row{$row['id']}">
        <td><a href="{$row['url']}" target="_blank">$urlTxt</a></td>
    </tr>
    <tr class="row{$row['id']}"  class="bottom_row">
        <td>{$row['summary']}</td>
    </tr>
EOD;
    }
    ?>  
</table>