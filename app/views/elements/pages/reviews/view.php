<?php

	$categoryId = $review['WpPost']['category_id'];
	$categoryNames = implode(' ', array_values($categoryId));
	$summary = preg_replace('/(\*\*(.+?)\*\*)/', '<b>『<i>$2</i>』</b>', $review['WpPost']['content']);

echo <<<EOD
		<div>{$categoryNames}</div>
		<div class="preview" style="display: block;">
			<!-- [ #container ] -->
			<div class="innerBox" id="container">
				<!-- [ #content ] -->
				<div class="content" id="content">
					<!-- [ #post- ] -->
					<h1 class="entryPostTitle entry-title">{$review['WpPost']['title']}</h1>
						{$summary}
				</div>
			</div>
		</div>
EOD;
