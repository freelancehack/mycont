<?php

	$rependingBtn = "<button class=\"repending\" type=\"button\">再レビュー</button>";
	$remandBtn = "<button class=\"remand\" type=\"button\">差し戻し</button>";
	$publishBtn = "<button class=\"publish\" type=\"button\">公開</button>";
	$deleteBtn = "<button class=\"close\" type=\"button\">却下削除</button>";

	$buttons = [];
	if ($isPartner) {
		switch ($reviewStatus) {
	    case "remand":
		    $buttons = [$rependingBtn];
	        break;
	    }
	}
	if ($isEditor) {
		switch ($reviewStatus) {
	    case "pending":
		    $buttons = [$remandBtn, $publishBtn, $deleteBtn];
	        break;
	    case "remand":
		    $buttons = [$publishBtn, $rependingBtn, $deleteBtn];
	        break;
	    case "repending":
		    $buttons = [$remandBtn, $publishBtn, $deleteBtn];
	        break;
		}	
	}
	if ($isAdmin) {
		switch ($reviewStatus) {
	    case "publish":
		    $buttons = [$remandBtn, $deleteBtn];
	        break;
	    case "close":
		    $buttons = [$remandBtn];
	        break;
	    }
	}
	echo join(' ', $buttons);
