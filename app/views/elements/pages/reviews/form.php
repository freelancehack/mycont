<?php

$contentRaw = $review['WpPost']['content_raw'];

preg_match('!\[fh_capture(| url=\'(.*?)\')\](.*?)\[/fh_capture\].*?\[fh_referral\](.*?)\[/fh_referral\]!s', $contentRaw, $match);

$url = $match[3] ? $match[3] : $match[2];

$checkedNews = '';
$checkedBookmark = '';
$checkedNonCapture = '';
	$categoryId = $review['WpPost']['category_id'];
if (array_key_exists(49, $categoryId)) {
$checkedNews = 'checked';

}
if (array_key_exists(44, $categoryId)) {
$checkedBookmark = 'checked';

}

$opacity = '';
if (empty($match[3])) {
	$opacity = 'opacity_30';
	$checkedNonCapture = 'checked';
}
$title = htmlspecialchars($review['WpPost']['title'], ENT_QUOTES);
$summary = htmlspecialchars($match[4], ENT_QUOTES);

echo <<<EOD
		<div>
			<input type="checkbox" value="49" name="category_id[]" id="cid49_{$id}" {$checkedNews}>
			<label for="cid49_{$id}">News</label>
			<input type="hidden" value="44" name="category_id[]">
			<input type="checkbox" value="44" name="category_id[]" id="cid44_{$id}" checked disabled>
			<label for="cid44_{$id}">ブックマーク</label>
		</div>
		<div class="preview" style="display: block;">
			<!-- [ #container ] -->
			<div class="innerBox" id="container">
				<!-- [ #content ] -->
				<div class="content" id="content">
					<!-- [ #post- ] -->
					<h1 class="entryPostTitle entry-title">
						<textarea name="title">{$title }</textarea>
					</h1>
					<input type="hidden" name="url" value="{$url}">
					<div class="center capture {$opacity}">
						<a href="{$url}">
						<img src="{$scdomain}capture/{$url}/320x240/square.png">
						</a>
						<p>スクリーンショット {$url}</p>
					</div>
					<input type="checkbox" name="noncapture" value="1" id="noncapture{$id}" {$checkedNonCapture}>
					<label for="noncapture{$id}">キャプチャ非掲載</label>
					<textarea name="summary">{$summary}</textarea>
				</div>
			</div>
		</div>
EOD;
