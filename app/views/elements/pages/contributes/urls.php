<?php

foreach ($candidates as $v) {
    $id = $v['id'];
    $formName = "form{$id}";
    $title = htmlspecialchars($v['title'], ENT_QUOTES);
    $summary = htmlspecialchars($v['summary'], ENT_QUOTES);
    $newsChecked = ($v['category_id'] == 49) ? 'checked' : '';
    $baseUrl = TPF_BASE_URL;
    $publishBtn = (!$isEditor && !$isAllowedPublish) ? '' : <<<EOD
    <input type="button" value="投稿" class="submit" data-form-name="{$formName}"> | 
EOD;

    echo <<<EOD
<form action="{$baseUrl}/contributes/" id="{$formName}"
    data-endisabled-url="{$tbaseUrl}/candidates/change?id={$id}&column=review_status&value=disabled">
    <input type="hidden" name="id" value="{$id}"  id="id{$id}">
    <table class="form">
        <tr>
            <th rowspan="6" class="index">{$id}</th>
            <td>
                <a href="{$v['url']}" class="open_url" target="_blank">紹介URL:</a><br>
                <input class="capture" type="text" name="url" value="{$v['url']}" readonly>
            </td>
        </tr>
            <td>{$summary}</td>
        </tr>
        </tr>
            <td>
                <input type="checkbox" name="category_id[]" value="49" id="cid49_{$id}" {$newsChecked}>
                <label for="cid49_{$id}">News</label>
                <input type="hidden" name="category_id[]" value="44">
                <input type="checkbox" name="category_id[]" value="44" id="cid44_{$id}" checked disabled>
                <label for="cid44_{$id}">ブックマーク</label>
            </td>
        </tr>
        <tr>
            <td><h2>タイトル:</h2>
                <textarea name="title" id="title_{$id}" rows="3">{$title}</textarea><br>
            </td>
        </tr>
        </tr>
            <td><h2>要約:</h2><textarea name="summary" id="summary_{$id}" rows="7"></textarea></td>
        </tr>
        </tr>
            <td class="confirm_button">
                <input type="button" value="確認" class="confirm" data-form-name="{$formName}">
            </td>
        </tr>
    </table>
    <div class="preview">
        <div class="content"></div>
        <div>
            <input type="checkbox" name="noncapture" value="1"  id="noncapture{$id}">
            <label for="noncapture{$id}">キャプチャ非掲載</label><br>
        </div>
        <div class="message"></div>

    </div>
    <div class="buttons">
        {$publishBtn}
        <input type="button" value="レビュー依頼" class="pennding" data-form-name="{$formName}">
    </div>
</form>

EOD;
}
    /*
    echo "<a 
                href='{$v['url']}' 
                title='{$title}' 
                target='_blank' 
                data-id='{$id}' 
                data-category_id='{$v['category_id']}' 
                data-summary='{$v['summary']}'
                data-publish_date='{$v['publish_date']}'
                >{$title}</a><br/>";
                */
