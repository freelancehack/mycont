<table class="history_table">
	<tr>
		<th>id</th>
		<th>時刻</th>
		<th>作業内容</th>
		<th>作業者</th>
		<th></th>
	</tr>

	<caption><?php echo $caption; ?></caption>
<?php
	if(count($histories)) foreach ($histories as $history) {
		if (empty($history['fh_bookmarks_wp_post_id'])) {
			$href = TPF_BASE_URL.
				'/candidates/?id='. $history['fh_bookmark_id'];
		} else {
			$href = TPF_BASE_URL.
				'/bookmark_info/?id='. $history['fh_bookmark_id'];
		} 
		echo <<<EOD
	<tr>
		<td>{$history['id']}</td>
		<td>{$history['created']}</td>
		<td>{$history['fh_tasks_name']}</td>
		<td>{$history['wp_users_display_name']}</td>
		<td><a href="{$href}">{$history['fh_bookmark_id']}</a></td>			
	</tr>
EOD;
	}
?>
</table>
