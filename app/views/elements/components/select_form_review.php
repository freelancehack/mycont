<?php


//        $partnerUsers = $this->FhBookmarkDao->partnerUser($targetUserIds);





	$statusOptions = $userOptionsUl = '';

	$bookmarkObj = new Bookmark();
	$reviewStatuses = $bookmarkObj->reviewStatuses;
       
    foreach ($reviewStatuses as $v => $label) {
        if ($request['review_status'] == $v) {
	        $statusOptions .= '<option value="'.$v.'" selected>'.$label.'</option>';
            continue;
	    }
        $statusOptions .= '<option value="'.$v.'">'.$label.'</option>';
    }

    if ($isEditor) {
    	$userOptions = '';
        if (!empty($request['id'])) {
        	$userOptions .= '<option value=""></option>';
        }
        foreach ($partnerUsers as $i => $v) {
            if ($request['user_id'] == (string)$v['user_id']) {
                $userOptions .= '<option value="'.$v['user_id'].'" selected>'.$v['display_name'].'</option>';
                continue;
            }
            $userOptions .= '<option value="'.$v['user_id'].'">'.$v['display_name'].'</option>';
        }

$userOptionsUl = <<<EOD
		<li>ユーザー：
			<select name="user_id">
				<option value="">全て</option>
				{$userOptions}
			</select>
		</li>
EOD;

    }


	echo <<<EOD

<form id="conditions">
	<ul class="select_form">
		<li>ステータス：
			<select name="review_status">
				<option value="">全て</option>
				{$statusOptions}
			</select>
		</li>
		{$userOptionsUl}
	</ul>
</form>

EOD;

