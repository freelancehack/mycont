<div class="paginate">
	<ul>
		<li>
			<?php echo $pagination['total']; ?> 件中 <?php echo $pagination['current']; ?> 
        </li>
        <?php if (!empty($pagination['pages']['prev'])) : ?>
            <li class="btn">
                <a href="<?php echo $pagination['pages']['prevUrl']; ?>">前へ</a>
            </li>
        <?php endif; ?>
        <?php if (!empty($pagination['pages']['next'])) : ?>
            <li class="btn">
                <a href="<?php echo $pagination['pages']['nextUrl']; ?>">次へ</a>
            </li>
        <?php endif; ?>
        <?php if (!empty($pagination['pages']['last'])) : ?>
            <li class="btn">
                <a href="<?php echo $pagination['pages']['lastUrl']; ?>">最後へ</a>
            </li>
        <?php endif; ?>
    </ul>
</div>

