<?php

	$userId = $userData['ID'];
	$crawler_id = $request['crawler_id'];
	$category_id = $request['category_id'];

	$keyWordOptions = $categoryOptions = '';

	$bookmarkObj = new Bookmark();
	$crawlers = $bookmarkObj->crawlers;
	$usedCategoryIds = $bookmarkObj->usedCategoryIds;

	foreach ($crawlers as $i => $v) {
		if ($crawler_id == (string)$i) {
			$keyWordOptions .= '<option value="'.$i.'" selected>'.$v.'</option>';
			continue;
		}
		$keyWordOptions .= '<option value="'.$i.'">'.$v.'</option>';
    }
	foreach ($usedCategoryIds as $i => $v) {
		if ($category_id == (string)$i) {
			$categoryOptions .= '<option value="'.$i.'" selected>'.$v.'</option>';
			continue;
		}
		$categoryOptions .= '<option value="'.$i.'">'.$v.'</option>';
	}
	$userIdChk = '';
	if ($request['user_id'] == $userId) {
		$userIdChk = 'checked';
	}
	$recommendedChk = '';
	if ($request['recommended'] == 1) {
		$recommendedChk = 'checked';
	}

	echo <<<EOD

<form id="conditions">
	<ul class="select_form">
		<li>収集キーワード：
			<select name="crawler_id">
				<option value="">全て</option>
				{$keyWordOptions}
			</select>
		</li>
		<li>投稿カテゴリ：
			<select name="category_id">
				<option value="">全て</option>
				{$categoryOptions}
			</select>
		</li>
	</ul>
	<ul class="select_form">
		<li>
			<input type="checkbox" value="{$userId}" name="user_id" id="user_id_chk" {$userIdChk}>
			<label for="user_id_chk">自分の担当のみ</label>	
		</li>
		<li>
			<input type="checkbox" value="1" name="recommended" id="recommended_chk" {$recommendedChk}>
			<label for="recommended_chk">推薦</label>	
		</li>
	</ul>
</form>

EOD;

