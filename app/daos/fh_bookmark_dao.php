<?php

class FhBookmarkDao extends AppDao
{
    protected $table = 'fh_bookmarks';

    protected $simpleFkeys = [
        'user_id' => ['wp_users.id', 'display_name']
    ];


    public function candidates($request, $calc = false)
    {
        $query = $this->candidatesQuery($request, $calc);
        return $this->execute($query);
    }

    private function candidatesQuery($request, $calc)
    {
        $offset = ($request['page'] - 1) * $this->div;
        unset($request['page']);
        if (empty($request['user_id'])) {
            $request['user_id'] = [$this->linkContributerUserID, $this->userID];
        }
        $conditions = [];
        foreach ($request as $key => $value) {
            $sign = '=';
            if (is_array($value)) {
                $sign = 'in';
                $value = '( '. join(', ', $value). ' )';
            }
            $condition = ($value !== null && $value !== '') ? "fh_bookmarks.{$key} {$sign} {$value}" : '';
            if  ($condition) {
                $conditions[] = $condition;
            }
        }
        $andWhere = '';
        if (count($conditions)) {
            $andWhere = 'AND '. join(' AND ', $conditions);
        }
        $page_condition = "LIMIT {$this->div} OFFSET {$offset}";
        if ($calc) {
            $calc = ' SQL_CALC_FOUND_ROWS ';
        }
        $order = ($request['category_id'] == 49) ? 'ASC' : 'DESC';
        $query = $this->db()->prepare(
            "
                SELECT {$calc}
                    fh_bookmarks.*,
                    wp_users.display_name
                FROM
                    fh_bookmarks
                LEFT JOIN
                    wp_users
                ON
                    wp_users.ID = fh_bookmarks.user_id
                WHERE 
                    fh_bookmarks.review_status is NULL
                AND
                    fh_bookmarks.id NOT IN (
                        SELECT fh_bookmark_id
                        FROM fh_pass_candidates
                        WHERE user_id = %d
                    )

                {$andWhere}

                ORDER BY fh_bookmarks.publish_date {$order}, fh_bookmarks.id
                    {$page_condition} ;
            ",
            $this->userID
        );

        return $query;
    }

    public function registCandidateByDateByCrawlerId($crawlerId = null, $endDate = null, $termDays = 31)
    {
        if (empty($endDate)) {
            $endDate = current_time('Y-m-d');
        }
        $date = new DateTime($endDate);
        $date->sub(new DateInterval("P{$termDays}D"));
        $startDate = $date->format('Y-m-d');

        $query = 
            "
            SELECT
                virtual.target_date,
                bookmarks.total_of_day

            FROM
            (
                SELECT
                @f:='{$startDate}' as target_date
                UNION ALL
                SELECT
                @f:=DATE_ADD(@f, INTERVAL 1 DAY)
                FROM
                fh_bookmarks
                WHERE
                @f <= '{$endDate}'
            ) AS virtual

            LEFT JOIN
                (
                SELECT
                    count(id) AS total_of_day,
                    DATE_FORMAT(created, '%Y-%m-%d') AS d,
                    crawler_id
                FROM fh_bookmarks
                WHERE DATE_FORMAT(created, '%Y-%m-%d') BETWEEN '{$startDate}' AND '{$endDate}'
                GROUP BY
                    DATE_FORMAT(created, '%Y-%m-%d')
                ) AS bookmarks
            ON
                virtual.target_date = bookmarks.d

        ";
        return $this->execute($query);
    }

    public function candidateStatusByDateByCondition($andWhere = null, $endDate = null, $termDays = 31)
    {
        if (empty($endDate)) {
            $endDate = date_i18n('Y-m-d');
        }
        $date = new DateTime($endDate);
        $date->sub(new DateInterval("P{$termDays}D"));
        $startDate = $date->format('Y-m-d');

        $query = 
            "
            SELECT
                virtual.target_date,
                bookmarks.total_of_day

            FROM
            (
                SELECT
                @f:='{$startDate}' as target_date
                UNION ALL
                SELECT
                @f:=DATE_ADD(@f, INTERVAL 1 DAY)
                FROM
                fh_bookmarks
                WHERE
                @f <= '{$endDate}'
            ) AS virtual

            LEFT JOIN
                (
                SELECT
                    count(id) AS total_of_day,
                    DATE_FORMAT(created, '%Y-%m-%d') AS d
                FROM fh_bookmarks
                WHERE DATE_FORMAT(created, '%Y-%m-%d') BETWEEN '{$startDate}' AND '{$endDate}'
                {$andWhere}
                GROUP BY
                    DATE_FORMAT(created, '%Y-%m-%d')
                ) AS bookmarks
            ON
                virtual.target_date = bookmarks.d

        ";
        return $this->execute($query);
    }


    public function onReview($request, $calc = false)
    {
        $query = $this->onReviewQuery($request, $calc);
        return $this->execute($query);
    }

    private function onReviewQuery($request, $calc)
    {
        $offset = ($request['page'] - 1) * $this->div;
        unset($request['page']);
        if (!empty($request['sort'])) {
            $orderBy = $this->orderStmt($request['sort']);
        }
        unset($request['sort']);
        $conditions = ['fh_bookmarks.review_status is NOT NULL'];
        foreach ($request as $key => $value) {
            if (!empty($value)) {
                if (is_array($value)) {
                    $value = join("', '", $value);
                    $conditions[] = "fh_bookmarks.{$key} in ('{$value}')";
                } elseif (is_string($value)) {
                    $conditions[] = "fh_bookmarks.{$key} = '$value'";
                }
            }
        }
        $where = 'WHERE '. join(' AND ', $conditions);

        $page_condition = "LIMIT {$this->div} OFFSET {$offset}";
        if ($calc) {
            $calc = ' SQL_CALC_FOUND_ROWS ';
        }

        $status = "'pending'";


        $query = $this->db()->prepare(
            "
                SELECT {$calc} fh_bookmarks.*, wp_users.display_name from fh_bookmarks
                LEFT JOIN
                    wp_posts
                ON
                    wp_posts.id = fh_bookmarks.wp_post_id
                LEFT JOIN
                    wp_users
                ON
                    wp_users.ID = fh_bookmarks.user_id
               
                {$where}

                {$orderBy}
                    {$page_condition} ;
            ",
            $this->userID
        );
        return $query;
    }

    public function partnerUser($targetUserIds = null)
    {
        $andWhere = null;
        if ($targetUserIds) {
            $andWhere = 'AND fh_bookmarks.user_id IN ('. join(', ', $targetUserIds). ')';
        }
        return $this->db()->get_results(
            "
                SELECT
                    fh_bookmarks.user_id,
                    wp_usermeta.meta_value,
                    wp_users.display_name
                FROM fh_bookmarks
                LEFT JOIN
                    wp_usermeta
                ON
                    wp_usermeta.user_id = fh_bookmarks.user_id
                    AND
                    wp_usermeta.meta_key = 'wp_capabilities'
                LEFT JOIN
                    wp_users
                ON
                    wp_users.ID = fh_bookmarks.user_id
                WHERE
                    fh_bookmarks.user_id <> {$this->linkContributerUserID}
                    {$andWhere}
                GROUP BY fh_bookmarks.user_id
            ",
            ARRAY_A
        );
    }

    /**
     * 指定日を基準にnewsからbookmarkに変更される候補の数
     */
    public function previousNews($targetCreatedDate, $targetPublishDate)
    {
        $where = $this->previousNewsWhere($targetCreatedDate, $targetPublishDate);
        $query =
            "
                SELECT id, crawler_id
                FROM {$this->table}
                {$where}
            ";
        return $this->execute($query);
    }
    /**
     * 指定日を基準にnewsからbookmarkに変更する
     */
    public function changePreviousNewsToBookmark($targetCreatedDate, $targetPublishDate)
    {
        $where = $this->previousNewsWhere($targetCreatedDate, $targetPublishDate);
        $query =
            "
                UPDATE {$this->table}
                SET 
                    category_id = 44
                {$where}
            ";
        return $this->db()->get_var($query);       
    }

    private function previousNewsWhere($targetCreatedDate, $targetPublishDate)
    {
        return "
                WHERE
                    category_id = 49
                AND
                    review_status IS NULL
                AND
                    user_id = {$this->linkContributerUserID}
                AND
                (
                    DATE_FORMAT(created, '%Y-%m-%d') <= '{$targetCreatedDate}'
                OR
                    DATE_FORMAT(publish_date, '%Y-%m-%d') <= '{$targetPublishDate}'
                )
        ";

    }

    public function getExpired($execDateTime)
    {
         $query =
            "
                SELECT *
                FROM {$this->table}
                WHERE
                    reserved_expire <= '{$execDateTime}'
            ";
        return $this->execute($query);       
       
    }

    public function countOwn($userId)
    {
        $query =
            "
                SELECT
                    count(id)
                FROM
                    {$this->table}
                WHERE
                    user_id = {$userId}
                AND
                    reserved_expire IS NOT NULL
            ";
        return $this->db()->get_var($query);  
    }


    public function oldestNewsDate()
    {
        $query =
            "
                SELECT DATE_FORMAT(created, '%Y-%m-%d')
                FROM {$this->table}
                WHERE
                    category_id = 49
                ORDER BY created ASC
                LIMIT 1;
            ";
        return $this->db()->get_var($query);  

    }

    public function disabledDomains($request)
    {

        if ($request['domain']) {
            $where = "{$this->table}.domain = '{$request['domain']}'";
        } else {
            if ($request['review_status'] == 'null') {
                $where = "review_status is null";
            } else {
                $where = "review_status = '{$request['review_status']}'";
            }
            if (empty($request['reject_source'])) $request['reject_source'] = '0'; 
            $where .= " AND fh_source_domains.reject_source in ({$request['reject_source']})";
        }
        $query =
            "
                SELECT
                    {$this->table}.domain,
                    count({$this->table}.id) as cnt,
                    fh_source_domains.id as fh_source_domain_id,
                    fh_source_domains.reject_source,
                    P.pcnt
                FROM {$this->table}
                LEFT JOIN
                    fh_source_domains
                ON
                    fh_source_domains.domain = {$this->table}.domain
                LEFT JOIN
                    (
                        SELECT domain, count(id) AS pcnt
                        FROM {$this->table}
                        WHERE review_status = 'publish'
                        GROUP BY domain
                    ) AS P
                ON
                    P.domain = {$this->table}.domain
                WHERE
                    {$where}
                GROUP BY domain
                ORDER BY cnt DESC
                LIMIT 300;
            ";
        return $this->execute($query);       
    }
    public function raws($request)
    {
        $tmp = [];
        foreach ($request as $key => $value) {
            if ($value === null) {
                continue;
            }
            $tmp[] = "{$key} = '{$value}'";
        }
        $conditions = implode(' AND ', $tmp);
        $query =
            "
                SELECT *
                FROM {$this->table}
                WHERE
                {$conditions}
            ";
        return $this->execute($query);       


    }

    public function countRecommneded()
    {
         $query =
            "
                SELECT
                    count(id)
                FROM
                    {$this->table}
                WHERE
                    recommended = 1
                AND
                    review_status IS NULL
            ";
        return $this->db()->get_var($query);  
       
    }

}
