<?php

class FhPassCandidateDao extends AppDao
{
    protected $table = 'fh_pass_candidates';

    protected $simpleFkeys = [
        'user_id' => ['wp_users.id']
    ];

    public function regist($id, $userId){
		$data = [
            'user_id' => $userId,
            'fh_bookmark_id' => $id,
        ];
        return $this->insert($data);
    }

}