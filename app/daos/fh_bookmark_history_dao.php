<?php

class FhBookmarkHistoryDao extends AppDao
{
    protected $table = 'fh_bookmark_histories';

    protected $simpleFkeys = [
        'user_id' => ['wp_users.id', 'display_name'],
        'fh_task_id' => ['fh_tasks.id', 'name'],
        'fh_bookmark_id' => ['fh_bookmarks.id', 'wp_post_id'],
    ];

    protected $slackUrl = [
        'log-prod-candidates' => 'https://hooks.slack.com/services/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',
        'log-dev-candidates' => 'https://hooks.slack.com/services/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',
        'log-prod-reviews' => 'https://hooks.slack.com/services/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',
        'log-dev-reviews' => 'https://hooks.slack.com/services/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',
    ];

    public function insert($data, $cast = null)
    {
        $insertId = false;
        if ($this->db()->insert($this->table, $data, $cast)) {
            $insertId = $this->lastInsertId();
        }

        $channel = 'log-'. TPF_ENV. '-candidates';
        if (in_array($data['fh_task_id'], [2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16])) {
            $channel = 'log-'. TPF_ENV. '-reviews';
            $review = true;

        }
        $url = $this->slackUrl[$channel];

        if ($insertId) {
            $query = 
                "
                    SELECT
                        fh_tasks.name,
                        wp_users.display_name,
                        wp_posts.post_title,
                        fh_bookmarks.title,
                        fh_bookmarks.wp_post_id,
                        owner.display_name as owner_name
                    FROM
                        {$this->table}
                    LEFT JOIN
                        fh_tasks
                    ON
                        fh_tasks.id = {$this->table}.fh_task_id
                    LEFT JOIN
                        wp_users
                    ON
                        wp_users.ID = {$this->table}.user_id
                    LEFT JOIN
                        fh_bookmarks
                    ON
                        fh_bookmarks.id = {$this->table}.fh_bookmark_id
                    LEFT JOIN
                        wp_posts
                    ON
                        fh_bookmarks.wp_post_id = wp_posts.id
                    LEFT JOIN
                        wp_users as owner
                    ON
                        owner.ID = fh_bookmarks.user_id
                    WHERE
                        {$this->table}.id = {$insertId}

                ";
            $result = $this->execute($query);
            $text = <<<EOD
:hamster: *{$result[0]['display_name']} さんの作業* id : $insertId
>担当 : {$result[0]['owner_name']}
>タスク : {$result[0]['name']}
EOD;
            $title = null;
            if (!empty($result[0]['title'])) {
                $title = "\n>タイトル : ". $result[0]['title'].'(元記事)';
            }
            if (!empty($result[0]['post_title'])) {
                $title = "\n>タイトル : ". $result[0]['post_title'];
            }
            $text .= $title;
            if (!empty($review)) {
                if (!empty($data['params'])) {
                    $paramsSerialized = $data['params'];
                    $params = unserialize($paramsSerialized);
                    if (!empty($params['request']['comment'])) {
                        $text .= "\n>コメント : ". $params['request']['comment'];
                    }
                }
            }
            if (in_array($data['fh_task_id'], [2, 6, 10, 14])) {
                $text .= "\n". home_url(). "/bkmks/reviews/?id=". $data['fh_bookmark_id'];
            }
            if (in_array($data['fh_task_id'], [3, 4, 7, 11, 12, 15])) {
                $text .= "\n". home_url(). "/?p=". $result[0]['wp_post_id'];
            }
            if ($data['fh_task_id'] == 25) {
                $paramsSerialized = $data['params'];
                $params = unserialize($paramsSerialized);
                $text .= "\n". $params['request']['domain']. ' の候補 '. count($params['request']['targetIds']). '件をdisabledにしました。';
            }

        } else {
            $text = "予期せぬエラーです\n". serialize($data);
        }
        $content = [
            'channel' => $channel,
            'username' => '作業ログ',
            'text' => $text,
            'icon_emoji' => ':dog:'
        ];
        $json = json_encode($content);

        $cmd = "nohup curl -X POST {$url} -d 'payload={$json}' > /dev/null &";
        exec($cmd, $output);

        return $insertId;
    }

    public function myHistoriesByMonth($request, $calc)
    {
        $offset = ($request['page'] - 1) * $this->div;

        $page_condition = "LIMIT {$this->div} OFFSET {$offset}";

        $postQuery = "
            AND 
                DATE_FORMAT({$this->table}.created, '%Y-%m') = '{$request['target_month']}'
            ORDER BY id DESC
            {$page_condition}
        ";
        return $this->getByUserId($request['user_id'], $postQuery, true);
    }

    public function getWorkMonths($userId = null) {
        if ($userId) {
            $where = "WHERE user_id = {$userId}";
        }
        $query = 
            "
                SELECT DATE_FORMAT(created, '%Y-%m') AS month

                FROM fh_bookmark_histories
                {$where}
                GROUP BY month
                ORDER BY month DESC
            ";
        return $this->execute($query);

    }

    public function getWorkUserIds($targetMonth = null)
    {
        if ($targetMonth) {
            $andWhere = "AND DATE_FORMAT({$this->table}.created, '%Y-%m') = {$targetMonth}";
        }
        $query = 
            "
                SELECT {$this->table}.user_id
                FROM fh_bookmark_histories
                LEFT JOIN 
                    wp_users
                ON 
                    wp_users.ID = {$this->table}.user_id
                WHERE
                    wp_users.ID IS NOT NULL
                {$andWhere}
                GROUP BY  {$this->table}.user_id
                ORDER BY  {$this->table}.user_id DESC
            ";
        return $this->execute($query);        

    }

    public function logDelete($categoryId, $userId, $request, $ret)
    {
        $data = [
            'user_id' => $userId,
            'fh_bookmark_id' => $request['id'],
            'fh_task_id' => ($categoryId == 49) ? 1 : 9, // delete
            'params' => serialize(compact('request', 'ret'))
        ];
        $insetId = $this->insert($data);
    }

    public function logRegist($categoryIds, $userId, $postType, $request, $insertId, $update)
    {
        $tasId = ($postType == 'publish') ? 7 : 2;
        if (!in_array('49', $categoryIds)) $tasId = $tasId + 8;
        $data = [
            'user_id' => $userId,
            'fh_bookmark_id' => $request['id'],
            'fh_task_id' => $tasId, // pending or publish
            'params' => serialize(compact('request', 'insertId', 'update'))
        ];
        $insetId = $this->insert($data);
    }
    
    public function logRemand($categoryIds, $userId, $request)
    {
        $data = [
            'user_id' => $userId,
            'fh_bookmark_id' => $request['id'],
            'fh_task_id' => !empty($categoryIds[49]) ? 5 : 13, // remand
            'params' => serialize(compact('request'))            
        ];
        $insetId = $this->insert($data);
    }
    public function logRepending($categoryIds, $userId, $request)
    {
        $data = [
            'user_id' => $userId,
            'fh_bookmark_id' => $request['id'],
            'fh_task_id' => !empty($categoryIds[49]) ? 6 : 14, // repending
            'params' => serialize(compact('request'))            
        ];
        $insetId = $this->insert($data);
    }

    public function logPublish($categoryIds, $userId, $ownerId, $request, $recommended)
    {
        $id = $request['id'];
        $data = [
            'user_id' => $userId,
            'fh_bookmark_id' => $id,
            'fh_task_id' => !empty($categoryIds[49]) ?3 : 11, // publish
            'params' => serialize(compact('request'))            
        ];

        $insetId = $this->insert($data);
        if ($recommended) {
            $data = [
                'user_id' => $userId,
                'fh_bookmark_id' => $id,
                'fh_task_id' => 20,
            ];
            $insetId = $this->insert($data);
        }

        // 他人の投稿の場合だけはbe published を記録する
        if ($userId != $ownerId) {
            $data = [
                'user_id' => $ownerId,
                'fh_bookmark_id' => $id,
                'fh_task_id' => !empty($categoryIds[49]) ? 4: 12, // be published
            ];
            $insetId = $this->insert($data);
            // recommended加算
            if ($recommended) {
                $data = [
                    'user_id' => $ownerId,
                    'fh_bookmark_id' => $id,
                    'fh_task_id' => 21, // be published
                ];
                $insetId = $this->insert($data);
            }
        }
    }
    public function logClose($categoryIds, $userId, $request)
    {
        $data = [
            'user_id' => $userId,
            'fh_bookmark_id' => $request['id'],
            'fh_task_id' => !empty($categoryIds[49]) ? 8 : 16, // close
            'params' => serialize(compact('request'))            
        ];
        $insetId = $this->insert($data);
    }

    public function logRemovePreviousNews($targetNews, $targetNewsCount, $targetCreatedDate, $targetPublishDate, $specifiedCreated)
    {
        $data = [
            'user_id' => $this->linkContributerUserID,
            'fh_task_id' => 17,
            'params' => serialize(compact('targetNews', 'targetNewsCount', 'targetCreatedDate', 'targetPublishDate'))
        ];
        if ($specifiedCreated) $data['created'] = $specifiedCreated;
        $insetId = $this->insert($data);
    }


    public function existLogReserve($id, $userId)
    {
        $query =
            "
                SELECT
                    COUNT(id)
                FROM {$this->table}
                WHERE
                    user_id = {$userId}
                AND 
                    fh_bookmark_id = {$id}
                AND 
                    fh_task_id = 18

            ";
        return $this->db()->get_var($query);
    }

    public function logReserve($id, $userId)
    {
        $data = [
            'user_id' => $userId,
            'fh_bookmark_id' => $id,
            'fh_task_id' => 18,
        ];
        $insetId = $this->insert($data);
    }

    public function logCancelReserved($id, $userId)
    {
        $data = [
            'user_id' => $userId,
            'fh_bookmark_id' => $id,
            'fh_task_id' => 19,
        ];
        $insetId = $this->insert($data);
    }

    public function removedPreviousNews($startDate, $endDate)
    {
        $query =
            "
                SELECT *
                FROM {$this->table}
                WHERE
                    fh_task_id = 17
                AND
                    DATE_FORMAT(created, '%Y-%m-%d') BETWEEN '{$startDate}' AND '{$endDate}'

            ";
        return $this->execute($query);
    }
    public function logRecommended($id, $userId)
    {
        $data = [
            'user_id' => $userId,
            'fh_bookmark_id' => $id,
            'fh_task_id' => 24
        ];
        $insetId = $this->insert($data);

    }
        public function logPassCandidate($id, $userId)
    {
        $data = [
            'user_id' => $userId,
            'fh_bookmark_id' => $id,
            'fh_task_id' => 23
        ];
        $insetId = $this->insert($data);

    }
    public function logDisabledByDomain($request, $userId)
    {
        $data = [
            'user_id' => $userId,
            'fh_task_id' => 25,
            'params' => serialize(compact('request'))            

        ];
        $insetId = $this->insert($data);

    }

}
