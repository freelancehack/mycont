<?php

class FhPartnerFeeDao extends AppDao
{
    protected $table = 'fh_partner_fees';


    public function getByUserId($userId, $targetMonth = null)
    {
        if (!$targetMonth) {
            $targetMonth = date_i18n('Y-m');
        }
        $query = "

			SELECT * FROM 
    		(
    			SELECT
					fee.fh_task_id,
					u.ID as user_id,
					u.display_name,
					t.name,
					t.editor,
					t.order,
				    fee.fee,
				    ifnull(h.cnt, 0) as cnt,
				    ifnull((fee.fee * h.cnt), 0) as total,
				    fee.effective_date as effective_date
				FROM fh_partner_fees as fee

				LEFT JOIN
				(
					SELECT
						fh_task_id,
						count(id) as cnt
					FROM
						fh_bookmark_histories
					WHERE
						user_id = {$userId}
						AND  DATE_FORMAT(created, '%Y-%m') = '{$targetMonth}'
					GROUP BY fh_task_id
				) as h
				ON
				h.fh_task_id = fee.fh_task_id

				LEFT JOIN
					fh_tasks as t 
				ON 
					t.id = fee.fh_task_id

				LEFT JOIN
					wp_users as u 
				ON 
					u.ID = {$userId}

				WHERE
					fee.user_id = 

				(
					SELECT user_id FROM fh_partner_fees
					WHERE user_id in (0 , {$userId})
					group by user_id order by user_id desc limit 1
				)
				ORDER BY fee.effective_date desc

			) as t
			WHERE t.effective_date <='{$targetMonth}-01'
			GROUP BY t.fh_task_id
			ORDER BY t.order
		";
        return $this->db()->get_results($query, ARRAY_A);
    }

/*
fee.effective_date = 
(
	SELECT effective_date FROM fh_partner_fees
	WHERE user_id in (0 , {$userId})
	AND effective_date < '{$targetMonth}-01'
	group by user_id order by effective_date desc limit 1)*/
}
