<?php

class WpUsermetaDao extends AppDao
{
    protected $table = 'wp_usermeta';

    public function description($userId)
    {
    	$query =
    		"
    			SELECT meta_value
    			FROM {$this->table}
    			WHERE user_id = {$userId} AND meta_key = 'description'
    		";
    	return $this->db()->get_var($query);
    }
}