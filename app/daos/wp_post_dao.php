<?php

class WpPostDao extends AppDao
{
    protected $table = 'wp_posts';

    public function existBookmarkedPost($url)
    {
        $query = $this->db()->prepare(
            "
                SELECT * from wp_posts
                WHERE
                    post_content REGEXP %s
                    OR post_content REGEXP %s
            ",
            '\[fh_capture\][[:space:]]*'. $url. '[[:space:]]*\[/fh_capture\]',
            '\[fh_capture[[:space:]]*url=.'. $url. '.\]'
        );

        return $this->db()->get_results($query);
    }

    public function insertPost($request, $postType)
    {
        $post = [
            'post_content'   => $this->bookmarkPost($request),
            'post_name'      => $request['title'],
            'post_title'     => $request['title'],
            'post_status'    => $postType,
            'post_type'      => 'post',
            'post_author'    => $this->linkContributerUserID,
            'post_category'  => $request['category_id']
        ];
        return wp_insert_post($post);
    }

    public function postsByIds($ids)
    {
        $ret = [];
        $query = new WP_Query(['post__in' => array_keys($ids), 'post_status' => ['publish', 'pending']]);

        if ($query->have_posts()) {
            $data = [];
            while ($query->have_posts()) {
                $query->the_post();
                global $post;
                $categoryIds = [];
                foreach (get_the_category() as $value) {
                    $categoryIds[$value->cat_ID] = $value->name;
                }
                $data['category_id'] = $categoryIds;
                $data['ID'] = $post->ID;
                $data['post_author'] = $post->post_author;
                $data['post_status'] = $post->post_status;
                $data['title'] = get_the_title();
                // TODO ?
                ob_start();
                the_content();
                $data['content'] = ob_get_clean();
                $data['content_raw'] = get_the_content();
                $data['fh_bookmark_id'] = $ids[$post->ID];
                $ret[] = $data;
            }
            return $ret;
        } else {
            return false;
        }
    }

    public function categoryIdsByID($id)
    {
        $categoryIds = [];
        foreach (get_the_category($id) as $value) {
            $categoryIds[$value->cat_ID] = $value->name;
        }
        return $categoryIds;
    }

    public function updateByRepending($ID, $request)
    {
        $update = [
            'ID'             => $ID,
            'post_content'   => $this->bookmarkPost($request),
            'post_name'      => $request['title'],
            'post_title'     => $request['title'],
            'post_status'    => 'pending',
            'post_category'  => $request['category_id']
        ];
        return wp_update_post($update);
    }
    public function updateByPublish($ID)
    {
        $update = [
            'ID'             => $ID, 
            'post_status'    => 'publish',
        ];
        return wp_update_post($update);
    }

        /**
     * 投稿コンテンツの生成
     */
    private function bookmarkPost($request)
    {
        if (!empty($request['noncapture'])) {
            $capture = "[fh_capture url='{$request['url']}'][/fh_capture]";
        } else {
            $capture = "[fh_capture]{$request['url']}[/fh_capture]";
        }
        $content = <<<HTML
{$capture}
[fh_referral]
{$request['summary']}[/fh_referral]
HTML;
        return $content;
    }

}
