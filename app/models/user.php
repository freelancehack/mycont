<?php

class User extends AppModel
{
    public $useDaos = ['WpUsermetaDao', 'FhPartnerFeeDao', 'FhBookmarkDao', 'FhBookmarkHistoryDao'];


    public function getPartnerIds()
    {
        $userMetas = $this->WpUsermetaDao->findByMetaKey('wp_user_level');
        $ret = [];
        if ($userMetas) foreach ($userMetas as $meta) {
        	if ($meta['meta_value'] == '0') {
        		$ret[] = $meta['user_id'];
        	}
        }
        return $ret;
    }


    public function getStatusByUserId($userId, $request)
    {
        return [
            [
                'fee' => $this->FhPartnerFeeDao->getByUserId($userId, $request['target_month']),
                'workHistory' => $this->FhBookmarkHistoryDao->myHistoriesByMonth(array_merge($request,['user_id' => $userId]), true),
                'status' => $this->getBookmarkStatus($userId)
            ],
            [$userId => $this->FhBookmarkHistoryDao->getWorkMonths($userId)]
        ];
    }

    public function getStatusAllUser($request)
    {
        $workHistoryMonths = $ret = [];
        $userIds = $this->FhBookmarkHistoryDao->getWorkUserIds();
        foreach ($userIds as $index => $v) {
            list($ret[$v['user_id']], $tmpWorkHistoryMonths) = $this->getStatusByUserId($v['user_id'], $request);
            $workHistoryMonths[$v['user_id']] = $tmpWorkHistoryMonths[$v['user_id']];
        }
        return [$ret, $workHistoryMonths];
    }

    public function getBookmarkStatus($userId)
    {
        return [
            'owns' => $this->FhBookmarkDao->countOwn($userId),
            'remand' => $this->FhBookmarkDao->count(['user_id' => $userId, 'review_status' => 'remand']),
            'pending' => $this->FhBookmarkDao->count(['user_id' => $userId, 'review_status' => 'pending']),
            'repending' => $this->FhBookmarkDao->count(['user_id' => $userId, 'review_status' => 'repending']),
            'reserved_canceled' => $this->FhBookmarkHistoryDao->count([
                'user_id' => $userId,
                'fh_task_id' => '19',
                "DATE_FORMAT(created, '%Y-%m')" => date('Y-m')
            ])
        ];
    }

    public function isAllowedPublish($userid)
    {
        $description = $this->WpUsermetaDao->description($userid);
        if (preg_match('!allow\-publish!', $description)) {
            return true;
        }
        return false;
    }

    public function myWorkHistories($request, $userId)
    {
        $histories = $this->FhBookmarkHistoryDao->myHistoriesByMonth(array_merge($request,['user_id' => $userId]), true);
        return [
            $histories,
            $this->pagination($this->FhBookmarkHistoryDao->foundRows(), $this->FhBookmarkHistoryDao->div(), $request, count($histories)),
            $this->selectForm($request, $this->FhBookmarkHistoryDao->getWorkMonths($userId))
        ];

    }

        /*
     * TODO viewに移動
     */
    public function selectForm($request, $months)
    {

        $ret = [];
        $ret[] = '<form id="conditions">';
        $ret[] = '<ul class="select_form">';
        $ret[] = '<li>';
        $ret[] = '収集キーワード：';
        $ret[] = '<select name="target_month">';
        foreach ($months as $month) {
            $m = $month['month'];
            if ($m == $request['target_month']) {
                $ret[] = '<option value="'.$m.'" selected>'.$m.'</option>';
                continue;
            }
            $ret[] = '<option value="'.$m.'">'.$m.'</option>';
        }
        $ret[] = '</select>';
        $ret[] = '</li>';
        $ret[] = '</ul>';

        $ret[] = '</form>';
        return join("\n", $ret);
    }

}
