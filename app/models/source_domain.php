<?php

class SourceDomain extends AppModel
{
    public $useDaos = ['FhBookmarkDao', 'FhBookmarkHistoryDao', 'FhSourceDomainDao'];

    public function update($request, $userId)
    {
    	if (empty($request['id']) || $request['reject_source'] == '') {
    		return ['result' => 0];
    	}
    	$result = $this->FhSourceDomainDao->update(
    		['reject_source' => $request['reject_source']],
    		['id' => $request['id']]);

        if ($request['reject_source'] == 1 && $result == 1) {
            $resourceDomain = $this->FhSourceDomainDao->findById($request['id']);
            $domain = $resourceDomain[0]['domain'];
            $targets = $this->FhBookmarkDao->find('id', "WHERE domain = '{$domain}' AND review_status IS NULL");
            $request['domain'] = $domain;
            if (count($targets)) {
                $request['targetIds'] = [];
                foreach ($targets as $target) {
                    $this->FhBookmarkDao->update(
                        ['review_status' => 'disabled'],
                        ['id' => $target['id']]);
                    $request['targetIds'][] = $target['id'];
                }
                $this->FhBookmarkHistoryDao->logDisabledByDomain($request, $userId);

            }
        }

        return ['result' => $result];
    }

}