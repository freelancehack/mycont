<?php

class configure
{

    public $categories;
    public $crawlerType = ['hand', 'フリーランス', '個人事業主', '副業'];

    function __construct()
    {
        $this->setCategories();
    }
    function setCategories()
    {
        $args = array(
        'orderby' => 'order',
        'order' => 'ASC',
        'exclude' => '1' // 「未設定」カテゴリを除外

        );
        //$this->categories = get_categories($args);
    }

    function categoryName($id)
    {
        return get_category($id, ARRAY_A)['cat_name'];
    }
    function crawlerTypeName($id)
    {
        return $this->crawlerType[$id];
    }
}
