<?php

class BookmarkCandidate extends Bookmark
{

    /**
     * ブックマーク候補を追加する
     */
    public function registCandidate($request)
    {
        if (count($this->WpPostDao->existBookmarkedPost($request['url']))) {
            return [null, 'WordPressの記事中にすでにデータが存在します(ドラフトの場合もあります。)'];
        }

        if (count($this->FhBookmarkDao->findByUrl($request['url']))) {
            return [null, '候補がすでに存在します。'];
        }

        $insertId = $this->FhBookmarkDao->insert(array_merge($request, ['user_id' => Auth::userID()]));

        if ($insertId) {
            return [$insertId, null];
        }

        return [null, '登録ができませんでした。'];
    }
    /*
     * 候補をパス
     */
    public function pass($request, $userId)
    {
        $ret = $this->isAllowedOperation($request['id'], $userId);
        if (!$ret['result']) {
            return $ret;
        }
        $result = $this->FhPassCandidateDao->regist($request['id'], $userId);
        if ($result) {
            $this->FhBookmarkHistoryDao->logPassCandidate($request['id'], $userId);
        }
        return [
            'result' => (int)(bool)$result,
            'message' => null
        ];
    }

    /*
     * 候補の1カラムを変更
     */
    public function change($request, $userId)
    {
        // validation
        $ret = $this->isAllowedOperation($request['id'], $userId);
        if (!$ret['result']) {
            return $ret;
        }

        $sets = [$request['column'] => $request['value']];
        $conditions = ['id' => $request['id']];
        $casts = ['%d'];
        if (!Auth::isAdmin()) {
            // TODO 自分が登録した候補の場合
            $conditions = array_merge($conditions, ['user_id' => $this->FhBookmarkDao->linkContributerUserID]);
            $casts = array_merge($casts, ['%d']);
            if ($request['column'] === 'review_status' && $request['value'] === 'disabled') {
                $sets = array_merge($sets, ['user_id' => $userId]);
            }
        }
        // 候補をupdate
        $result = $this->FhBookmarkDao->update($sets, $conditions);
        // 削除履歴を記録
        if ($result && $request['column'] === 'review_status' && $request['value'] === 'disabled') {
            $categoryId = $this->FhBookmarkDao->findCategoryIdById($request['id']);
            $this->FhBookmarkHistoryDao->logDelete($categoryId[$request['id']], $userId, $request, $ret);
        }
        // 削除履歴を記録
        if ($result && $request['column'] === 'recommended' && $request['value']) {
            $this->FhBookmarkHistoryDao->logRecommended($request['id'], $userId, $request, $ret);
        }
        return [
            'result' => $result,
            'message' => null
        ];
    }
	
    /**
     * 指定の日にちで、newsからbookmarkに変更する
     */
    public function changeNewsToBookmark($targetCreatedDate, $targetPublishDate, $specifiedCreated = false)
    {
        $targetNews = $this->FhBookmarkDao->previousNews($targetCreatedDate, $targetPublishDate);
        $targetNewsCount = [];
        if (!empty($targetNews)) foreach ($targetNews as $value) {
            if (empty($targetNewsCount[$value['crawler_id']]))  $targetNewsCount[$value['crawler_id']] = 0;
            $targetNewsCount[$value['crawler_id']]++;
        }
        $this->FhBookmarkDao->changePreviousNewsToBookmark($targetCreatedDate, $targetPublishDate);
        $this->FhBookmarkHistoryDao->logRemovePreviousNews(
            $targetNews, $targetNewsCount, $targetCreatedDate, $targetPublishDate, $specifiedCreated
        );
    }

    public function reserve($request, $userId)
    {
        $ret = $this->isAllowedOperation($request['id'], $userId);
        if (!$ret['result']) {
            return $ret;
        }
        $exist = $this->FhBookmarkHistoryDao->existLogReserve($request['id'], $userId);
        if ($exist) {
            return [
                'result' => 0,
                'errorMessage' => '担当付けができるのは一つの候補につき1度限りです'
            ];
        }
        $update = $this->FhBookmarkDao->update(
            [
                'user_id' => $userId,
                'reserved_expire' => date_i18n('Y-m-d H:i:s', strtotime('1 days 9 hours'))
            ],
            [
                'id' => $request['id'],
                'user_id' => $this->FhBookmarkDao->linkContributerUserID
            ]
        );
        if ($update) {
            $this->FhBookmarkHistoryDao->logReserve($request['id'], $userId);
            return ['result' => 1];
        }
        return [
            'result' => 0,
            'errorMessage' => 'update処理に失敗しました'
        ];
    }

    public function cancelReserved($execDateTime)
    {
        $FhBookmarkId = [];
        $target = $this->FhBookmarkDao->getExpired($execDateTime);
        if ($target) foreach ($target as $index => $value) {
            $FhBookmarkId[] =  $value['id'];
            $update = $this->FhBookmarkDao->update(
                ['reserved_expire' => null, 'user_id' => $this->FhBookmarkHistoryDao->linkContributerUserID],
                ['id' => $value['id']]
            );
            if ($update) {
                $this->FhBookmarkHistoryDao->logCancelReserved($value['id'], $value['user_id']);
            }
        }
        var_dump($execDateTime, $FhBookmarkId);

    }

    public function oldestNewsDate()
    {
        return $this->FhBookmarkDao->oldestNewsDate();
    }

    public function countRecommneded()
    {
        return $this->FhBookmarkDao->countRecommneded();
    }

}