<?php

class BookmarkInfo extends Bookmark
{
	public function get($request)
	{
		$fhBookmark = $this->FhBookmarkDao->getById($request['id']);
		$wpPost = $this->WpPostDao->getById($fhBookmark[0]['wp_post_id']);
		$histories = $this->FhBookmarkHistoryDao->getByFhBookmarkId($request['id'], 'ORDER BY id DESC');
		return [$fhBookmark, $wpPost, $histories];
	}
}
    