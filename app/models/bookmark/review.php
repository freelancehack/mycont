<?php

class BookmarkReview extends Bookmark
{
    /*
     * 利用モデル
     */
    public $useDaos = ['FhBookmarkDao', 'WpPostDao', 'FhBookmarkHistoryDao'];


    public function onReviews($request)
    {
        $fhBookmarks = $this->FhBookmarkDao->onReview($request, true);
        $totalRows = $this->FhBookmarkDao->foundRows();
        $pagination = $this->pagination($totalRows, $this->FhBookmarkDao->div(), $request, count($fhBookmarks));

        if (count($fhBookmarks) === 0) {
            return [null, $pagination];
        }


        $wpPostIds = [];
        $ret = [];
        foreach ($fhBookmarks as $fhBookmark) {
            $wpPostIds[$fhBookmark['wp_post_id']] = $fhBookmark['id'];
            $ret[$fhBookmark['id']]['FhBookmark'] = $fhBookmark;
            $ret[$fhBookmark['id']]['FhBookmarkHistory']
                = $this->FhBookmarkHistoryDao->getByFhBookmarkId($fhBookmark['id'], ' ORDER BY ID DESC');
        }
        $wpPosts = $this->WpPostDao->postsByIds($wpPostIds);
        if (!empty($wpPosts)) foreach ($wpPosts as $wpPost) {
            $fhBookmarkId = $wpPost['fh_bookmark_id'];
            $ret[$fhBookmarkId]['WpPost'] = $wpPost;
        }

        return [$ret, $pagination];
    }

    public function change($request, $userId)
    {
        $fhBookmarkId = $request['id'];
        $toStatus =  $request['to_status'];
        $wpPostId = $this->FhBookmarkDao->findWpPostIdById($fhBookmarkId);
        if ($toStatus == 'repending') {
            if (!$this->WpPostDao->updateByRepending($wpPostId[$fhBookmarkId], $request)) {
        return [
            'result' => 0,
            'request' => $request
        ];

            }
        } elseif ($toStatus == 'publish') {
            $this->WpPostDao->updateByPublish($wpPostId[$fhBookmarkId]);            
        }
        if (empty($fhBookmarkId) || empty($toStatus)) {
            return ['result' => 0];
        }
        $ret = $this->changeReviewStatus($fhBookmarkId, $toStatus);
        if (!empty($ret)) {
            $fhBookmark = $this->FhBookmarkDao->findById($fhBookmarkId);
            $categoryIds = $this->WpPostDao->categoryIdsByID($fhBookmark[0]['wp_post_id']);

            switch ($toStatus) {
                case 'remand':
                    $logRet = $this->FhBookmarkHistoryDao->logRemand($categoryIds, $userId, $request);
                    break;
                case 'repending':
                    $logRet = $this->FhBookmarkHistoryDao->logRepending($categoryIds, $userId, $request);
                    break;
                case 'publish':
                    $logRet = $this->FhBookmarkHistoryDao->logPublish($categoryIds, $userId, $fhBookmark[0]['user_id'], $request, $fhBookmark[0]['recommended']);
                    break;
                case 'close':
                    $logRet = $this->FhBookmarkHistoryDao->logClose($categoryIds, $userId, $request);
                    break;
            }

        }
        return [
            'result' => $ret,
            'log' => $logRet,
            'request' => $request
        ];
    }
    /*
     * 候補の1カラムを変更
     */
    public function changeReviewStatus($fhBookmarkId, $reviewStatus)
    {
        $sets = ['review_status' => $reviewStatus];
        $conditions = ['id' => $fhBookmarkId];
        if (!Auth::isEditor() && $reviewStatus !== 'repending') {
            echo 'error not repending';
            exit;
        }

        return $this->FhBookmarkDao->update($sets, $conditions);

        return $ret;
    }


    public function partnerUser($targetUserIds)
    {

        return $this->FhBookmarkDao->partnerUser($targetUserIds);
    }
}
