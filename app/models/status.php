<?php

class Status extends AppModel
{
    public $useDaos = ['FhBookmarkDao', 'FhBookmarkHistoryDao', 'WpPostDao'];

    public function crawlingStatus($endDate = null, $termDays = 31)
    {

        $transitions['created'] = $this->FhBookmarkDao->registCandidateByDateByCrawlerId(null, $endDate, $termDays);

        $max = 0;
        $chartData = [];
        foreach ($transitions['created'] as $dayData) {
            $chartData['labels'][] = $this->labelDate($dayData['target_date']);
                $chartData['values'][0][] = $dayData['total_of_day'] ? $dayData['total_of_day'] : 0;
            if ($dayData['total_of_day'] > $max) {
                $max = $dayData['total_of_day'];
            }
        }
        if (!empty($chartData['values'][0])) {
            $max = ceil($max);
            $max += 5 - ($max % 5); // Increase max to number that is evenly divisible by 5
            $step = $max / 5;
            return [
                'data' => $chartData,
                'min' => 0,
                'max' => $max,
                'step' => $step
            ];
        }

    }

	public function workStatus($endDate = null, $termDays = 31)
    {
        $transitions['disabled'] = $this->FhBookmarkDao->candidateStatusByDateByCondition(" AND review_status = 'disabled'", $endDate, $termDays);
        $transitions['closed'] = $this->FhBookmarkDao->candidateStatusByDateByCondition(" AND review_status = 'closed'", $endDate, $termDays);
        $transitions['pending'] = $this->FhBookmarkDao->candidateStatusByDateByCondition(" AND review_status = 'pending'", $endDate, $termDays);
        $transitions['repending'] = $this->FhBookmarkDao->candidateStatusByDateByCondition(" AND review_status = 'repending'", $endDate, $termDays);
        $transitions['remand'] = $this->FhBookmarkDao->candidateStatusByDateByCondition(" AND review_status = 'remand'", $endDate, $termDays);
        $transitions['publish'] = $this->FhBookmarkDao->candidateStatusByDateByCondition(" AND review_status = 'publish'", $endDate, $termDays);
        $transitions['candidate_subwork'] = $this->FhBookmarkDao->candidateStatusByDateByCondition(' AND review_status IS NULL AND crawler_id = 3', $endDate, $termDays);
        $transitions['candidate_solo'] = $this->FhBookmarkDao->candidateStatusByDateByCondition(' AND review_status IS NULL AND crawler_id = 2', $endDate, $termDays);
        $transitions['candidate_freelance'] = $this->FhBookmarkDao->candidateStatusByDateByCondition(' AND review_status IS NULL  AND crawler_id = 1', $endDate, $termDays);
        $data = [];
        $ticks = [];
        foreach ($transitions as $type => $val) {
        	if (!count($ticks)) {
	        	foreach ($val as $index => $value) {
	        		$ticks[$index + 1] = $this->labelDate($value['target_date']);
	        	}
        	}
        	$tmpData = [];
        	foreach ($val as $index => $value) {
        		$data[$type][$index + 1] = (int)$value['total_of_day'];

        	}
        }
        return [ 'data' => $data, 'ticks' => $ticks];
    }

    public function outdatedNewsStatus($endDate = null, $termDays = 31)
    {
        if (empty($endDate)) {
            $endDate = current_time('Y-m-d');
        }
        $date = new DateTime($endDate);
        $date->sub(new DateInterval("P{$termDays}D"));
        $startDate =  $date->format('Y-m-d');

        $crawler = ['candidate_subwork' => 3, 'candidate_solo' => 2, 'candidate_freelance' => 1];
        $transitionAllData = $this->FhBookmarkHistoryDao->removedPreviousNews($startDate, $endDate);
        $data = [];
        $ticks = [];
        $tmp = [];
        foreach ($transitionAllData as $value) {
            $params = unserialize($value['params']);
            foreach ($crawler as $name => $crawlerId) {
                if (empty($tmp[$name][$params['targetCreatedDate']] )) {
                    $tmp[$name][$params['targetCreatedDate']] =
                        !empty($params['targetNewsCount'][$crawlerId]) ? $params['targetNewsCount'][$crawlerId] : 0;
                } else {
                    $tmp[$name][$params['targetCreatedDate']] +=
                        !empty($params['targetNewsCount'][$crawlerId]) ? $params['targetNewsCount'][$crawlerId] : 0;
                }
                
            }
        }
        $tickIndex = 1;
        for($i = $termDays; $i > 0 ; $i--) {
            $tickTermDays = $i + 8;
            $date = new DateTime($endDate);
            $date->sub(new DateInterval("P{$tickTermDays}D"));
            $targetDate =  $date->format('Y-m-d');
            $ticks[$tickIndex] = $this->labelDate($targetDate);
            foreach ($crawler as $name => $crawlerId) {
                $data[$name][$tickIndex] =
                    !empty($tmp[$name][$targetDate]) ? $tmp[$name][$targetDate] : 0 ;
            }            
            $tickIndex++;
        }
        return [ 'data' => $data, 'ticks' => $ticks];


    }

    private function labelDate($date) {
        list($y, $m, $d) = explode('-', $date);
        $label = ' ';
        if ($d === '01') {
            $label = "$y/$m/$d";
        } elseif (preg_match('!5|10|20!', $d)) {
            $label = $d;
        }

        return $label;
    }


    public function disabledDomains($request)
    {
        return $this->FhBookmarkDao->disabledDomains($request);
    }
}
