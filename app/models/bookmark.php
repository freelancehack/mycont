<?php

class Bookmark extends AppModel
{
    /*
     * 利用モデル
     */
    public $useDaos = ['FhBookmarkDao', 'WpPostDao', 'FhBookmarkHistoryDao', 'FhPassCandidateDao'];

    /**
     * クローラー定義 TODO いずれ外出しかDB定義に
     */
    public $crawlers = array(
        '無し(手入力)',
        'フリーランス',
        '個人事業主',
        '副業',
    );

    /**
     * 利用しているカテゴリ
     */
    public $usedCategoryIds = array(
        44 => 'ブックマーク',
        49 => 'news'
    );
    public $reviewStatuses = [
        'pending' => 'レビュー待ち',
        'remand' => '差し戻し',
        'repending' => '再レビュー待ち',
        'publish' => '公開済み',
        'close' => '却下削除',
        'disabled' => '削除'
    ];

    /**
     * 候補取得
     */
    public function candidates($request)
    {
        $candidates = $this->FhBookmarkDao->candidates($request, true);
        $totalRows = $this->FhBookmarkDao->foundRows();
        $pagination = $this->pagination($totalRows, $this->FhBookmarkDao->div(), $request, count($candidates));
        return [$candidates, $pagination];
    }
    /**
     * URLを含む投稿が存在するかどうか
     */
    public function existBookmarkedPost($url)
    {
        return $this->WpPostDao->existBookmarkedPost($$url);
    }
    /**
     * WPPOSTへの登録
     */
    public function registBookmark($request, $postType, $userId)
    {
        $bkmk = $this->FhBookmarkDao->getById($request['id']);
        if (!in_array($bkmk[0]['user_id'], [$userId, $this->FhBookmarkDao->linkContributerUserID])) {
            return [
                'status' => 'error',
                'message' => $bkmk[0]['wp_users_display_name']. ' さんの担当になっています。投稿はできません。'
            ];

        }
        $insertId = $this->WpPostDao->insertPost($request, $postType);
        if (!$insertId) {
            return [
                'status' => 'error',
                'message' => '登録エラー'
            ];
        }
        // 投稿 or レビュー
        $update = $this->FhBookmarkDao->update(
            [
                'wp_post_id' => $insertId,
                'user_id' => $userId,
                'review_status' => $postType,
                'reserved_expire' => null
            ],
            ['id' => $request['id']]
        );
        // 履歴を記録
        $this->FhBookmarkHistoryDao->logRegist($request['category_id'], $userId, $postType, $request, $insertId, $update);
        return [
            'status' => 'success',
            //'message' => "{$postType}: <a href='/?p={$insertId}&preview_id={$insertId}' target='_blank'>{$insertId} 保存しました</a>"
            'message' => "{$postType}: <a href=\"". TPF_BASE_URL. "/reviews?id={$request['id']}\" target=\"_blank\">保存しました</a>"
        ];
    }

    protected function isAllowedOperation($id, $userId)
    {
        // validation
        $bkmk = $this->FhBookmarkDao->getById($id);
        if (!in_array($bkmk[0]['user_id'], [$userId, $this->FhBookmarkDao->linkContributerUserID])) {
            return [
                'result' => false,
                'status' => 'error',
                'errorMessage' => $bkmk[0]['wp_users_display_name']. ' さんの担当になっています。操作はできません。'
            ];

        }
        return ['result' => 1];
    }
}
