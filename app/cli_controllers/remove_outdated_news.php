<?php

class RemoveOutdatedNewsController extends CliController
{

    public $useModels = ['Bookmark' => ['Candidate']];


	public function main($execDate = null, $specifiedCreatedFlag = false)
	{
		$specifiedCreated = null;
		if (!$execDate) {
			$execDate = date_i18n('Y-m-d');
		}
		if ($specifiedCreatedFlag) {
			$specifiedCreated = $execDate. ' 02:00:00';
		}

		$targetCreatedDate = date_i18n('Y-m-d', strtotime('-8 days 9 hours', strtotime($execDate)));
		$targetPublishDate = date_i18n('Y-m-d', strtotime('-15 days 9 hours', strtotime($execDate)));
		$this->BookmarkCandidate->changeNewsToBookmark($targetCreatedDate, $targetPublishDate, $specifiedCreated);
		echo $targetDate;
	}

	public function init()
	{
		$startDate = $this->BookmarkCandidate->oldestNewsDate();

		$datetime1 = new DateTime($startDate);
		$datetime2 = new DateTime(date_i18n('Y-m-d'));
		$interval = $datetime1->diff($datetime2);
		$diffDays = $interval->format('%a');

		$baseDir= TPF_BASE_PATH;
		for ($i = -$diffDays; $i < 1; $i++) {
			$execDate = date_i18n('Y-m-d', strtotime("{$i} days 9 hours"));
			$result = `echo '{$execDate}'`;
			echo $result;

			$result = `/usr/bin/php {$baseDir}index.php remove_outdated_news $execDate 1`;
			echo $result;
		}

	}
}