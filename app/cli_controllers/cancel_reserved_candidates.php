<?php

class CancelReservedCandidatesController extends CliController
{

    public $useModels = ['Bookmark' => ['Candidate']];


	public function main($execDateTime = null)
	{
		if (!$execDateTime) {
			$execDateTime = date_i18n('Y-m-d H:i:s');
		}
		$this->BookmarkCandidate->cancelReserved($execDateTime);
	}
}