<?php

class AppConfigure
{
    private $pathJs = [
    ];
    private $filesJs = [
        '_jqplot' => [
          'default/index',
          'graph/index',
        ],
    ];
   private $filesJsSet = [
    '_jqplot' => [
      '//cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/excanvas.min.js' ,
      '//cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/jquery.jqplot.min.js' ,
      '//cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.highlighter.min.js' ,
      '//cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.dateAxisRenderer.min.js' ,
      '//cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.canvasTextRenderer.min.js' ,
      '//cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.canvasAxisTickRenderer.min.js',
      '//cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.barRenderer.min.js',
      TPF_BASE_URL. '/assets/js/components/jqplot.render.js',
    ]
   ];



    private $pathCss = [
    ];
    private $filesCss = [
        '//cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/jquery.jqplot.min.css' => [
          'default/index',
          'graph/index',
        ],
    ];
   private $filesCssSet = [
   ];

   private $autoLinkFiles = [];
 
    public function __construct()
    {
      $this->initAutoLinkFiles();
    }
    public function initAutoLinkFiles()
    {
      $this->autoLinkFiles['js'] = $this->parseLinkFiles('Js');
      $this->autoLinkFiles['css'] = $this->parseLinkFiles('Css');
    }

    public function parseLinkFiles($type)
    {
      $path = "path{$type}";
      $files = "files{$type}";
      $filesSet = "files{$type}Set";

      $ret = $this->$path;
      foreach ($this->$files as $linkFile => $urls) {
        if (!empty($this->$filesSet[$linkFile])) {
          foreach ($urls as $url) {
            if (empty($ret[$url])) {
              $ret[$url] = [];

            }
            $ret[$url] = array_merge($ret[$url], $this->$filesSet[$linkFile]);
          }
        } else {
          foreach ($urls as $url) {
            if (empty($ret[$url])) {
            $ret[$url] = [$linkFile];

            } else {
            $ret[$url] = array_merge($ret[$url], [$linkFile]);
            }
          }

        }
      }
      return $ret;
    }
    public function autoLinkFiles()
    {
      return $this->autoLinkFiles;
    }
}
