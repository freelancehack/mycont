(function (global) {
    'use strict';
    var myApp = global.myApp || {};

    myApp.common = {
        click: null,
        init: function () {
            this.setClick();
            this.initGnavi();
            this.initNavi();

        },
        _ua: {
            Tablet: function() {
              var u = window.navigator.userAgent.toLowerCase();
              return
              (u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
              || u.indexOf("ipad") != -1
              || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
              || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
              || u.indexOf("kindle") != -1
              || u.indexOf("silk") != -1
              || u.indexOf("playbook") != -1;
            },
            Mobile: function() {
              var u = window.navigator.userAgent.toLowerCase();
              return
              (u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
              || u.indexOf("iphone") != -1
              || u.indexOf("ipod") != -1
              || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
              || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
              || u.indexOf("blackberry") != -1;
            }
        },
        setClick: function() {
            myApp.common.click = ('ontouchstart' in document) && myApp.common._ua.Mobile() ? 'touchstart' : 'click';
        },
        initGnavi: function () {
            $(".slide_btn").on(myApp.common.click , function () {
                $(this).toggleClass("active");
                $(".slide_box").slideToggle("fast");
            });

        },
        initNavi: function () {
            $("form#conditions select, form#conditions input").on('change', function () {
                var query = '';
                $.each($('form#conditions').find('select, input:checked'), function () {
                  query = query + '&' + $(this).attr('name') + '=' + $(this).val();
                });
                var reload = location.pathname + '?' + query;
                location.href=reload;
            });
        }



    }


    $(function () {
        myApp.common.init();
        global.click = myApp.common.click;
    });

})(window);