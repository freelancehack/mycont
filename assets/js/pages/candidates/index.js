(function (global) {
    'use strict';
    var myApp = global.myApp || {};

    myApp.candidates = {
        init: function () {
            $("td button").on(global.click , function () {
                var id = $(this).attr('data-id');
                var url = $(this).attr('data-href');
                var msg = $(this).attr('data-message');
                myApp.candidates.ajax(id, url, msg, $(this));
                return false;
            }); 

        },
        ajax: function (id, url, msg, $this) {
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-Update', 'ajax_complete');
                    var $class = $this.attr('class');
                    if ($class != 'reserve'
                        && $class != 'recommended') {
                        $('#id'+id).html('<td colspan="3">doing...</td>');
                        $('.row'+id).remove();
                        $this.parent().find('button').remove();
                    } 
                },
                error: function (xhr, textStatus, error) {
                    alert('予期せぬエラーです');
                },
                success: function (json) {
                    if (json.result == 1) {
                        var $class = $this.attr('class');
                        if ($class == 'reserve') {
                            $('#id'+id).removeClass();
                            $('#id'+id).find('td').eq(0).addClass('is_owner');
                            $('.display_name_'+id).text($this.attr('data-display_name'));
                            $this.parent().find('button').remove();
                        } else if ($class == 'recommended') {
                            $this.replaceWith('<br>' + msg);
                        } else {
                            $('#id'+id).find("td").text(msg);
                        }
                    } else {
                        alert(json.errorMessage || '予期しないエラーです');
                        if ($this.attr('class') != 'reserve') {
                            $('#id'+id).html('<td colspan="3">'+json.errorMessage+'</td>');
                        }
                    }
                }
            });
        }

    }


    $(function () {
        myApp.candidates.init();
    });
})(window);