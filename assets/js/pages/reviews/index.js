(function (global) {
    'use strict';
    var myApp = global.myApp || {};

    myApp.reviews = {
        init: function () {
            $(".remand, .publish, .close").on(global.click , function () {
                var toStatus = $(this).attr('class');
                var $form = $(this).parent().parent();
                $form.find('input[name=to_status]').val(toStatus);
                myApp.reviews.change($form, 'change_status');
            });

            $(".repending").on(global.click , function () {
                var toStatus = $(this).attr('class');
                var $form = $(this).parent().parent();
                $form.find('input[name=to_status]').val(toStatus);
                myApp.reviews.change($form, 'repending');
            });
            $('body').on('change', 'input[type=checkbox]', function () {
                var checked = $(this).prop('checked');
                if (checked) {
                    $(this).prev().addClass('opacity_30');
                } else {
                    $(this).prev().removeClass('opacity_30');                    
                }
            });
            $(".comment_area").on(global.click , function () {
                $(this).find('textarea').show();
            });

        },
        change: function ($form, action) {
            $.ajax({
                type: "POST",
                url: $form.prop('action') + action,
                dataType: 'text',
                data: $form.serialize(),
                dataType: 'json',

                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-Update', 'ajax_complete');
                },
                error: function (xhr, textStatus, error) {
                    console.log('error', error);
                },
                success: function (json) {
                    if (json.result && json.request.id) {
                        $('#review_' + json.request.id).html(
                            'change status to '
                            + '<a href="?id=' + json.request.id + '">' +json.request.to_status + '</a>'
                        )
                    }
                }
            });
        }
    }

    $(function () {
        myApp.reviews.init();
    });
})(window);
