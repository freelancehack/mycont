(function (global) {
    'use strict';
    var myApp = global.myApp || {};

    myApp.disabledDomains = {
        init: function () {
            $("input[type=radio]").on('change', function () {
                var $form = $(this).parent();
            	myApp.disabledDomains.change($form);
            });
        },
        change: function ($form) {
            $.ajax({
                type: "POST",
                url: 'update_disabled_domain',
                data: $form.serialize(),
                dataType: 'json',

                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-Update', 'ajax_complete');
                },
                error: function (xhr, textStatus, error) {
                    alert('予期しないエラーです' + error);
                },
                success: function (json) {
                }
            });
        }
    }

    $(function () {
        myApp.disabledDomains.init();
    });
})(window);
