(function (global) {
    'use strict';
    var sanitaize = {
        encode : function (str) {
            return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#39;');
        },

        decode : function (str) {
            return str.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&#39;/g, '\'').replace(/&amp;/g, '&');
        }
    };


    var myApp = global.myApp || {};

    myApp.contLink = {
        init: function () {
            var urlParams = this.countUrls();
            //this.contAppend(urlParams);
            $(".confirm").on(global.click , function () {
                var $form = $("#" + $(this).attr('data-form-name'));
                    // capture_urlの確認
                myApp.contLink.checkUrlAndPreview($form);
                
            });

            $(".pennding").on(global.click , function () {
                var $form = $("#" + $(this).attr('data-form-name'));
                myApp.contLink.contribute($form, 'to_review');
            });
            $(".submit").on(global.click , function () {
                var $form = $("#" + $(this).attr('data-form-name'));
                myApp.contLink.contribute($form, 'publish');
            });
            $(".disabled").on(global.click , function () {
                var $form = $("#" + $(this).attr('data-form-name'));
                myApp.contLink.endisabled($form);
            });

            $(".open_url").on(global.click , function () {
                var $input = $(this).next().next();
                console.log($input);
                $(this).attr("href", $input.val());
            });



            var agent = navigator.userAgent;
            if (agent.search(/iPhone/) != -1) {
                $('#content textarea').on('focus', function () {
                    var id = $(this).attr("id");
                    var text = $(this).val();
                    $("#leanModalTextarea").attr("data-id", id);
                    $("#leanModalTextarea").val(text);
                    $("#leanModalTextarea").focus();
                    $('a[rel*=leanModal]').click();
                });

                $('.modal_close').on(global.click , function () {
                    var id = $("#leanModalTextarea").attr("data-id");
                    var text = $("#leanModalTextarea").val();
                    $("#" + id).val(text);
                    console.log(id, text);
                });

                $('a[rel*=leanModal]').leanModal({
                    top: 10,                     // モーダルウィンドウの縦位置を指定
                    overlay : 0.5,               // 背面の透明度
                    closeButton: ".modal_close"  // 閉じるボタンのCSS classを指定
                });
            }

        },
        countUrls: function () {
            if ($('#urls').find('a').length == 0) {
                return [this.defaultParam];
            }
            var ret = [];
            $.each($('#urls').find('a'), function (i) {
                ret.push({
                    id: $(this).attr("data-id"),
                    index: i,
                    url: $(this).attr("href"),
                    title: $(this).attr("title"),
                    category_id: $(this).attr("data-category_id"),
                    publish_date: $(this).attr("data-publish_date"),
                    summary: $(this).attr("data-summary")
                });
            });
            return ret;

        },
        contAppend: function (ary) {
            $.each(ary, function (i, v) {
                var template = $("#form-tpl").html();
                template = template
                .replace(/{id}/g, v.id)
                .replace(/{publish_date}/g, v.publish_date)
                .replace(/{index}/g, i)
                .replace(/{indexNum}/g, i + 1);
                $("#content").append(template);
                var $form = $("#form" + String(i));
                $form.find("input[name=url]").val(v.url);
                $form.find("textarea[name=title]").val(v.title);
                $form.find("p.summary").html(v.summary);
                if (v.id == null) {
                    $form.find("input.disabled, span.disabled").hide();
                }
                if (v.category_id == 49) {
                    $form.find("input[value=49]").click();
                }
                $form.find(".confirm").on(global.click , function () {
                    // capture_urlの確認
                    myApp.contLink.checkUrlAndPreview($form);
                
                });
                $form.on("submit", function (e) {
                    e.preventDefault();
                })
            });
        },
        confirm: function ($form, data) {
            try {
                var json = $.parseJSON(data);
            } catch (e) {
                alert('サーバーエラーです');
                return;
            }
            $form.find(".submit, .pennding").show(1000);
            var postmain = $("#postmain-tpl").html()
            postmain = postmain
            .replace(/{url}/g, $form.find("input[name=url]").val())
            .replace(
                /{title}/,
                sanitaize.encode($form.find("textarea[name=title]").val())
            );
            $form.find(".preview .content").html(postmain);
            $.each($form.find("textarea[name=summary]").val().split(/\r\n|\r|\n/), function (i, v) {
                v = v.replace(/(\*\*(.+?)\*\*)/g, '<b>『<i>$2</i>』</b>');
                $form.find(".preview").find("ul").append('<li>'+v+'</li>');
            });
            if ($form.find("input[name=url]").val()) {
                $form.find(".preview").find("img").attr("src", $form.find(".preview").find("img").attr("data-url"));
            }
            if (json.status != 'success' || json.message != undefined) {
                $form.find(".preview .message").html(json.message);
            }

            $form.find(".preview").fadeIn(1000);
            $form.find(".buttons").fadeIn(1000);
        },
        checkUrlAndPreview: function ($form) {
            $.ajax({
                type: "POST",
                url: $form.prop('action') + 'confirm',
                dataType: 'text',
                data: $form.serialize(),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-Update', 'ajax_complete');
                    $form.find(".preview, .submit").fadeOut(100);
                },
                error: function (xhr, textStatus, error) {
                    console.log('error', error);
                },
                success: function (data) {
                    myApp.contLink.confirm($form, data);
                }
            });
        },
        contribute: function ($form, action) {
            $.ajax({
                type: "POST",
                url: $form.prop('action') + action,
                dataType: 'text',
                data: $form.serialize(),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-Update', 'ajax_complete');
                    $form.find(".buttons").html('loading...');
                },
                error: function (xhr, textStatus, error) {
                    console.log('error', error);
                },
                success: function (data) {
                    myApp.contLink.confirm($form, data);
                    $form.find(".buttons").html('finished');
                }
            });
        },
        endisabled: function ($form) {
            var url = $form.attr('data-endisabled-url');
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'text',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-Update', 'ajax_complete');
                },
                error: function (xhr, textStatus, error) {
                    console.log('error', error);
                },
                success: function (data) {
                    $form.fadeOut(100);
                    $form.html('論理削除しました');
                    $form.fadeIn(100);
                }
            });
        }



    }


    $(function () {
        myApp.contLink.init();
    });
})(window);
