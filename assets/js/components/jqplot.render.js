(function (global) {
    'use strict';
    var myApp = global.myApp || {};
    myApp.jqplot = myApp.jqplot || {};

    myApp.jqplot = {
        // 初期化処理
        init: function () {
            this.lineGraph();
            this.lineGraph2();
            this.barChart();
            this.barChart2();
            this.barChart3();
//            this.spResize();
//            this.appendPlot();
        },
        // ログイン前右カラムのシェアするボタン
        lineGraph: function () {


            $("#crawler-regist-graph")
            .jqplot(
                myGlobalVars.jqplot.lineGraph.data,
                {
                    title:{textColor:'#333333'},
                    grid:{shadow:false,drawBorder:false,borderColor:'#333333',background:'#F9F9F9'},
                    axesDefaults:{tickRenderer:$.jqplot.CanvasAxisTickRenderer,showTickMarks:true,rendererOptions:{drawBaseline:true,baselineWidth:1.5,baselineColor:'#333333'},tickOptions:{angle:-30,fontFamily:'Arial',fontSize:'7.5pt',textColor:'#333333',formatString:'%\'g'},autoscale:true},
                    axes:{
                        xaxis:{
                            renderer:$.jqplot.DateAxisRenderer,
                            rendererOptions:{tickInset:0.5},
                            ticks:myGlobalVars.jqplot.lineGraph.ticks                            
                        },
                        yaxis:{
                            min:myGlobalVars.jqplot.lineGraph.min,
                            max:myGlobalVars.jqplot.lineGraph.max,
                            tickInterval:myGlobalVars.jqplot.lineGraph.tickInterval
                        }
                    },
                    seriesDefaults:{lineWidth:2,showMarker:true,shadow:false,highlighter:{formatString:'%s'}},
                    highlighter:{show:true,tooltipAxes:'y',tooltipLocation:'e',sizeAdjust:3},
                    noDataIndicator:{show:true,indicator:'No data found',axes:{xaxis:{show:false},yaxis:{show:false}}},
                    seriesColors:['#6E96DE']
                }
            );

        },
        lineGraph2: function () {


            $("#crawler-regist-graph")
            .jqplot(
                myGlobalVars.jqplot.lineGraph.data,
                {
                    title:{textColor:'#333333'},
                    grid:{shadow:false,drawBorder:false,borderColor:'#333333',background:'#F9F9F9'},
                    axesDefaults:{tickRenderer:$.jqplot.CanvasAxisTickRenderer,showTickMarks:true,rendererOptions:{drawBaseline:true,baselineWidth:1.5,baselineColor:'#333333'},tickOptions:{angle:-30,fontFamily:'Arial',fontSize:'7.5pt',textColor:'#333333',formatString:'%\'g'},autoscale:true},
                    axes:{
                        xaxis:{
                            renderer:$.jqplot.DateAxisRenderer,
                            rendererOptions:{tickInset:0.5},
                            ticks:myGlobalVars.jqplot.lineGraph.ticks                            
                        },
                        yaxis:{
                            min:myGlobalVars.jqplot.lineGraph.min,
                            max:myGlobalVars.jqplot.lineGraph.max,
                            tickInterval:myGlobalVars.jqplot.lineGraph.tickInterval
                        }
                    },
                    seriesDefaults:{lineWidth:2,showMarker:true,shadow:false,highlighter:{formatString:'%s'}},
                    highlighter:{show:true,tooltipAxes:'y',tooltipLocation:'e',sizeAdjust:3},
                    noDataIndicator:{show:true,indicator:'No data found',axes:{xaxis:{show:false},yaxis:{show:false}}},
                    seriesColors:['#6E96DE']
                }
            );

        },
        barChart: function () {

            $("#jshandle__bar-chart")
            .jqplot(
                myGlobalVars.jqplot.barChart.data,
                {
                    title:{textColor:'#333333'},
                    grid:{shadow:false,drawBorder:false,borderColor:'#333333',background:'#FFF5EC'},
                    axesDefaults:{showTickMarks:true,rendererOptions:{drawBaseline:true,baselineWidth:1.5,baselineColor:'#333333'},tickOptions:{fontFamily:'Arial',fontSize:'8.5pt',textColor:'#333333',formatString:'%\'g'},autoscale:true},
                    axes:{
                        xaxis:{
                            renderer:$.jqplot.DateAxisRenderer,
                            rendererOptions:{tickInset:0.5},
                            ticks:myGlobalVars.jqplot.barChart.ticks
                        },
                        yaxis:{
                            min:myGlobalVars.jqplot.barChart.min,
                            max:myGlobalVars.jqplot.barChart.max,
                            tickInterval:myGlobalVars.jqplot.barChart.tickInterval
                        }
                    },
                    seriesDefaults:{lineWidth:2,showMarker:true,shadow:false,highlighter:{formatString:'%s'},rendererOptions:{barWidth:myGlobalVars.jqplot.barChart.barWidth}},
                    highlighter:{show:true,tooltipAxes:'y',tooltipLocation:'e',sizeAdjust:3},
                    noDataIndicator:{show:true,indicator:'No data found',axes:{xaxis:{show:false},yaxis:{show:false}}},
                    series:[{renderer:$.jqplot.BarRenderer,rendererOptions:{highlightColors:'#FEBD70'},highlighter:{formatString:'%s件'}}],
                    seriesColors:['rgba(254,189,112, 0.65)']
                }
            );

        },
        barChart2: function() {
            $("#review-status-gragh")
            .jqplot(
                myGlobalVars.jqplot.barChart2.data,
                {
                    stackSeries: true,
                    seriesDefaults: {
                        renderer: jQuery . jqplot . BarRenderer,
                    },
                    axes: {
                        xaxis: {
                            renderer: jQuery . jqplot . CategoryAxisRenderer,
                            ticks: myGlobalVars.jqplot.barChart2.ticks
                        }
                    },
                    series: myGlobalVars.jqplot.barChart2.series,
                    legend: {
                        show: true,
                        placement: 'inside',
                        location: 'ne',
                    }
                }
            );
        },
        barChart3: function() {
            $("#outdated-news-gragh")
            .jqplot(
                myGlobalVars.jqplot.barChart3.data,
                {
                    stackSeries: true,
                    seriesDefaults: {
                        renderer: jQuery . jqplot . BarRenderer,
                    },
                    axes: {
                        xaxis: {
                            renderer: jQuery . jqplot . CategoryAxisRenderer,
                            ticks: myGlobalVars.jqplot.barChart3.ticks
                        }
                    },
                    series: myGlobalVars.jqplot.barChart3.series,
                    legend: {
                        show: true,
                        placement: 'inside',
                        location: 'ne',
                    }
                }
            );
        },
        spResize: function () {
            $(window).resize(function () {
            });
        },
        appendPlot: function () {
            var widthSize = $(window).width();
            if (widthSize < 700) {
            } else {
            }
            myApp.jqplot.lineGraph();
            myApp.jqplot.barChart();
        }
    }

    $(function () {
        myApp.jqplot.init();
    })
})(window);

