<?php

ini_set('display_errors', 1);

define('TPF_BASE_PATH', dirname(__FILE__). '/');
//
define('TPF_BASE_URL', '/bkmks');

define('TPF_CORE_DIR', TPF_BASE_PATH. 'core/');
// define('TPF_APP_DIR'. "{$tpf(TPF_BASE_PATH)}app/");

//
require_once(TPF_BASE_PATH. '/../wp-load.php');

$tpfEnv = [
	'https://freelance-hack.com' => ['env' => 'prod', 'debug' => 0],
	'http://fh-hack.net' =>  ['env' => 'dev', 'debug' => 1]
];

define('TPF_ENV', $tpfEnv[home_url()]['env']);

define('TPF_DEBUG', $tpfEnv[home_url()]['debug']);

//
require_once(TPF_CORE_DIR. 'bootstrap.php');


$c = new $tpfControllerName();
$c->fetch();
